﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class StatusCheckerServer2 : MonoBehaviour
{

    public static StatusCheckerServer2 Instance { get; private set; }
    public void Awake() { Instance = this; }

    Thread tcpListenerThread = null;
    string prevStato;
    public string Stato;
    bool stateChanged = false;

    public void cambiastato(string statonuovo)
    {
        Stato = statonuovo;
        if (Stato != prevStato)
        {
            prevStato = Stato;
            stateChanged = true;
        }
    }

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start()
    {
        tcpListenerThread = new Thread(() => ListenForMessages());
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();
        Stato = prevStato = "2";

       // Debug.Log("Test_ANT");
    }

    private void Update()
    {
        if (stateChanged)
        {
            stateChanged = false;
            Debug.Log("[" + Time.time.ToString() + "] STATE CHANGED: " + Stato);
        }
    }
    public void ListenForMessages()
    {
        TcpListener server = null;
        NetworkStream stream = null;
        int port = 49152;
        IPAddress localAddr = IPAddress.Parse("127.0.0.1");
        server = new TcpListener(localAddr, port);
        server.Start();

        Byte[] bytes = new Byte[256];
        String data = null;

        while (true)
        {

            //Debug.Log("Waiting for a connection... ");
            try
            {


                using (TcpClient client = server.AcceptTcpClient())
                {
                    //Debug.Log("Connected to M9 Client!");

                    data = null;

                    // Get a stream object for reading and writing
                    stream = client.GetStream();
                    StreamWriter writer = new StreamWriter(stream);

                    int i = 0;

                    // Loop to receive all the data sent by the client.
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        // Translate data bytes to a ASCII string.
                        data = Encoding.ASCII.GetString(bytes, 0, i);

                        byte[] msg = Encoding.ASCII.GetBytes(data);

                        //stream.Write(msg, 0, msg.Length);
                        if (!data.Equals("ping") && !data.Equals("request"))
                        {
                            ;// Debug.Log(String.Format("Received: {0}", data));
                        }

                        if (data.Equals("request"))
                        {
                            //Debug.Log("Received: 'request'");
                            
                            //Arduino tutto ok
                            writer.Write(Stato);

                            //Altri Stati...

                            writer.Flush();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            if (!server.Pending())
            {
                //Debug.Log("#### AWAIT 5 secondi #####");
                Thread.Sleep(5000);
            }
        }
    }

    private void OnApplicationQuit()
    {

        if (tcpListenerThread != null)
            tcpListenerThread.Abort();
    }
}

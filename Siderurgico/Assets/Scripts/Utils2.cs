﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class Utils2 : MonoBehaviour
{
    public SO_Utils so;
    public string utilsFileName;

    // There will be only one Instance of this class
    public static Utils2 Instance { get; private set; }
    public void Awake() {
        Instance = this;
        loadGameData();
    }

    void loadGameData()
    {
        //string filePath = Path.Combine("C:\\Resources\\Siderurgico\\", utilsFileName);
        string filePath = "C:\\Resources\\Siderurgico\\" + utilsFileName;

        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            string dataAsJson = File.ReadAllText(filePath);
            // Pass the json to JsonUtility, and tell it to create a GameData object from it
            JsonUtility.FromJsonOverwrite(dataAsJson, so);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }
    }
    private void Start()
    {
    }
    private void OnDestroy()
    {
        string dataAsJson = JsonUtility.ToJson(so, true);

        string filePath = Path.Combine(Application.dataPath, utilsFileName);
        File.WriteAllText(filePath, dataAsJson);
    }
}


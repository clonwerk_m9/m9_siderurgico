﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CesoiaCurrPoint : MonoBehaviour {
    Text currScoreText;
	public Animator pulseCanv;

    public void OnEnable()
    {
        EventAggregator.addPartialPoint.AddListener(OnAddPartialPoint);
    }

    public void OnDisable()
    {
        EventAggregator.addPartialPoint.RemoveListener(OnAddPartialPoint);
    }

    void OnAddPartialPoint(float currScore)
    {
		currScoreText.text = ((int)currScore).ToString();
		pulseCanv.SetTrigger ("pulse");
    }

    // Use this for initialization
    void Start () {
        currScoreText = GetComponent<Text>();


    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

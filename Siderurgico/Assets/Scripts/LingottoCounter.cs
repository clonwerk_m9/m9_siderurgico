﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LingottoCounter : MonoBehaviour
{
    public _tutorialType counterType;
    public Text t;
    int currCount = 0;
    int pointCount = 0;
    int currDestroyedCount = 0;

    public Transform LingotsRoot;
    public GameObject SingleLingot;
    public GameObject[] FullLingots;
    public GameObject[] LingotsSelections;
    public GameObject[] MissedLingots;

    void Start()
    {
        //Updatetext ();
        FullLingots = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];
        LingotsSelections = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];
        MissedLingots = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];

        for (int i = 0; i < Utils2.Instance.so.LINGOTTI_COUNTER; i++)
        {
            GameObject currLingot = Instantiate(SingleLingot, LingotsRoot);
            FullLingots[i] = currLingot.transform.GetChild(0).gameObject;
            LingotsSelections[i] = currLingot.transform.GetChild(1).gameObject;
            MissedLingots[i] = currLingot.transform.GetChild(2).gameObject;


            FullLingots[i].SetActive(false);
            MissedLingots[i].SetActive(false);
            if (i == 0)
                LingotsSelections[i].SetActive(true);
            else
                LingotsSelections[i].SetActive(false);
        }
    }

    public void OnEnable()
    {
        EventAggregator.lingCreated.AddListener(OnLingCreated);
        EventAggregator.lingDestroyed.AddListener(OnLingDestroyed);
        EventAggregator.leverPressedAtRightTime.AddListener(OnLeverPressedAtRightTime);
		EventAggregator.leverPressedWrongTime.AddListener(OnLeverPressedWrongTime);
    }

    public void OnDisable()
    {
        EventAggregator.lingCreated.RemoveListener(OnLingCreated);
        EventAggregator.lingDestroyed.RemoveListener(OnLingDestroyed);
        EventAggregator.leverPressedAtRightTime.RemoveListener(OnLeverPressedAtRightTime);
		EventAggregator.leverPressedWrongTime.RemoveListener(OnLeverPressedWrongTime);
    }

    void OnLingCreated(int id)
    {
        if ((id == Utils.LEVA_CROGIUOLO && counterType == _tutorialType.CROGIUOLO) ||
            (id == Utils.LEVA_TRENO && counterType == _tutorialType.TRENO) ||
            (id == Utils.LEVA_PRESSA && counterType == _tutorialType.PRESSA) ||
            (id == Utils.LEVA_CESOIA && counterType == _tutorialType.CESOIA))
        {
            //Debug.Log("OnLingCreated " + id);
            //NB: This check is mandatory only for Treno, and is performed in OnLeverPressedAtRightTime
            //if (currCount > Utils2.Instance.so.LINGOTTI_COUNTER) return;
            currCount++;
            UpdateVisual();
        }
    }

    void OnLeverPressedAtRightTime(int id)
    {
        if ((id == Utils.LEVA_CROGIUOLO && counterType == _tutorialType.CROGIUOLO) ||
            (id == Utils.LEVA_TRENO && counterType == _tutorialType.TRENO) ||
            (id == Utils.LEVA_PRESSA && counterType == _tutorialType.PRESSA) ||
            (id == Utils.LEVA_CESOIA && counterType == _tutorialType.CESOIA))
        {

            if (currCount == 0) return;
            //This is a mandatory check for id == Utils.LEVA_TRENO: currCount will be > LINGOTTI_COUNTER because in case of TRENO
            //  OnLingCreated is called by Animation state "ReturnBack"
            if (currCount > Utils2.Instance.so.LINGOTTI_COUNTER) return;
            //if (id == Utils.LEVA_TRENO)
            //    Debug.Log("OnLeverPressedAtRightTime TRENO: "+currCount);

            if (!MissedLingots[currCount - 1].activeSelf)
                FullLingots[currCount - 1].SetActive(true);
            EventAggregator.updateFinalScorePanels.Invoke();

            pointCount = 0;
            for (int i = 0; i < Utils2.Instance.so.LINGOTTI_COUNTER; i++)
            {
                if (FullLingots[i].active == true)
                    pointCount++;
            }
            if (t != null)
                t.text = pointCount.ToString();

        }
    }

	void OnLeverPressedWrongTime(int id)
	{
		if ((id == Utils.LEVA_CROGIUOLO && counterType == _tutorialType.CROGIUOLO) ||
			(id == Utils.LEVA_TRENO && counterType == _tutorialType.TRENO) ||
			(id == Utils.LEVA_PRESSA && counterType == _tutorialType.PRESSA) ||
			(id == Utils.LEVA_CESOIA && counterType == _tutorialType.CESOIA))
		{
            if (currCount == 0) return;
            if (currCount > Utils2.Instance.so.LINGOTTI_COUNTER) return;
            if(!FullLingots[currCount - 1].activeSelf)
                MissedLingots[currCount - 1].SetActive(true);
            //Cose da fare se questo lingotto è stato SBAGLIATO, ovvero NON E' STATO BENE
            //Debug.Log("OnLeverPressedWrongTime: "+id);
		}
	}
    /*
    void Updatetext ()
	{
		if(t != null)
		t.text = currCount.ToString ("00") + "/" + Utils2.Instance.so.LINGOTTI_COUNTER.ToString("00");
	}
    */

    void OnLingDestroyed(int id, float currScore)
    {
        if ((id == Utils.LEVA_CROGIUOLO && counterType == _tutorialType.CROGIUOLO) ||
            (id == Utils.LEVA_TRENO && counterType == _tutorialType.TRENO) ||
            (id == Utils.LEVA_PRESSA && counterType == _tutorialType.PRESSA) ||
            (id == Utils.LEVA_CESOIA && counterType == _tutorialType.CESOIA))
        {
            currDestroyedCount++;
            if (currDestroyedCount != Utils2.Instance.so.LINGOTTI_COUNTER)
                return;
            
            //NB: This will prevent game loop stuck on final game score display
            EventAggregator.partialGameEnded.Invoke(id);
        }
    }

    void UpdateVisual()
    {
        for (int i = 0; i < Utils2.Instance.so.LINGOTTI_COUNTER; i++)
        {

            if (i == currCount - 1)
            {
                LingotsSelections[i].SetActive(true);
            }
            else
                LingotsSelections[i].SetActive(false);

        }
      
    }
}
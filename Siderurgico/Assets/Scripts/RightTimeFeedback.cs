﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightTimeFeedback : MonoBehaviour {
    public float duration = 2;
    float timer;

    private void OnEnable()
    {
        timer = duration;        
    }

    void Update () {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
                gameObject.SetActive(false);
        }
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerDebug : MonoBehaviour {
    public KeyCode level01_CrogiuoloKey;
    public KeyCode level02_TrenoKey;
    public KeyCode level03_PressaKey;

    public float simulatedScore;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(level01_CrogiuoloKey))
            EventAggregator.lingDestroyed.Invoke(Utils.LEVA_CROGIUOLO, simulatedScore);
        else if (Input.GetKeyDown(level02_TrenoKey))
            EventAggregator.lingDestroyed.Invoke(Utils.LEVA_TRENO, simulatedScore);
        else if (Input.GetKeyDown(level03_PressaKey))
            EventAggregator.lingDestroyed.Invoke(Utils.LEVA_PRESSA, simulatedScore);
    }
}

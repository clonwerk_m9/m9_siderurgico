﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;

public class XMLReader : MonoBehaviour {
	public static XMLReader Instance { get; set; }

	XmlNodeList nodeList;
	XmlDocument XMLDoc;
	public string locFileName;

    void Awake()
    {
		if (Instance == null)
        {
			Instance = this;
        }
    }

	void Start(){
		LoadLocalizedText ();
	}

	public void LoadLocalizedText () {
		XMLDoc = new XmlDocument();
        XMLDoc.LoadXml(File.OpenText("C:\\Resources\\Siderurgico\\" + locFileName).ReadToEnd());

		// Load and create game data
		readLocFile();
	}

	void readLocFile(){
		nodeList = XMLDoc.GetElementsByTagName ("item");
		if (nodeList.Count == 0) {
			DebugXMLLog ("ERROR - No data to read");
			return;
		}

		foreach (XmlNode node in nodeList) {
			string key, value;
			if (node.ChildNodes [0].Name == "HANDLE") {
				key = node.ChildNodes [0].InnerText;
			} else {
				DebugXMLLog ("ERROR - HANDLE/HANDLE text bad formatting data");
				return;
			}
			if (node.ChildNodes [1].Name == "VALUE") {
				value = node.ChildNodes [1].InnerText;
			} else {
				DebugXMLLog ("ERROR - HANDLE/HANDLE text bad formatting data");
				return;
			}
            //Debug.Log("Adding "+ key+", "+value);
			LocalizationCtrl.Instance.texts.Add (key, value);
		}
	}

	void DebugXMLLog(string message){
		Debug.Log ("XML ERROR - " + message);
	}
}

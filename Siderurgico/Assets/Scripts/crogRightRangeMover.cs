﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crogRightRangeMover : MonoBehaviour
{
    public RectTransform rightRangeRoot;

    // There will be only one Instance of this class
    public static crogRightRangeMover Instance { get; private set; }
    public void Awake() { Instance = this; }

    public void updatePosX(float posX)
    {
        rightRangeRoot.anchoredPosition = new Vector2(posX+50, rightRangeRoot.anchoredPosition.y);
    }
}
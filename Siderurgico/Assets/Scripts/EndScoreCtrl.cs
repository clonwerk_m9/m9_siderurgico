﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScoreCtrl : MonoBehaviour {
    public Text TotScoreText, GrogiuoloScoreText, TrenoScoreText, PressaScoreText, CesoiaScoreText, EndTimerText;
    public Image CrogiuoloImg, TrenoImg, PresseImg, CesoiaImg;
    float TotScore, CrogiuoloScore, TrenoScore, PressaScore, CesoiaScore;
    float endTimer;
    bool updateEndTimer;

    private void Start()
    {
        endTimer = Utils2.Instance.so.END_RESTART_TIMER;
        updateEndTimer = false;
    }

    void OnEnable() {
        updateAllText();
        EventAggregator.partialGameEnded.AddListener(OnPartialGameEnded);
        EventAggregator.updateFinalScorePanels.AddListener(OnUpdateFinalScorePanels);
        EventAggregator.endGame.AddListener(OnEndGame);
    }

    public void OnDisable()
    {
        EventAggregator.partialGameEnded.RemoveListener(OnPartialGameEnded);
        EventAggregator.updateFinalScorePanels.RemoveListener(OnUpdateFinalScorePanels);
        EventAggregator.endGame.RemoveListener(OnEndGame);
    }

    void OnUpdateFinalScorePanels()
    {
        updateAllText();
    }
    void OnPartialGameEnded(int id)
    {
        updateAllText();
    }
    void OnEndGame()
    {
        updateEndTimer = true;
    }

    private void Update()
    {
        updateOnlyTotText();
        /*
        if (updateEndTimer) {
            endTimer -= Time.deltaTime;
            if (endTimer < 0)
                EventAggregator.gameRestart.Invoke();
            else {
                EndTimerText.text = endTimer.ToString("00");
            }
        }
        */
        EndTimerText.text = IdleWatcher.Instance.timer.ToString("00");
    }

    void updateAllText()
    {
        if (LevelCtrl.Instance != null)
        {
            //Fraction of right time pressed lever during the game
            TotScore = LevelCtrl.Instance.totScore;
            //if we want multiply everything to display percentage
            float mul = 100;
            /*CrogiuoloScore = (float)LevelCtrl.Instance.CrogiuoloScore + Utils2.Instance.so.CROGIUOLO_EMPTY_MINIMUMSCORE;
            CrogiuoloScore /= (float)(Utils2.Instance.so.LINGOTTI_COUNTER * Utils2.Instance.so.CROGIUOLO_SCORE_MAX);
            CrogiuoloScore *= mul;*/
            CrogiuoloScore = ((float)LevelCtrl.Instance.CrogiuoloScore / (float)Utils2.Instance.so.LINGOTTI_COUNTER) * mul;
            TrenoScore = ((float)LevelCtrl.Instance.TrenoScore / (float)Utils2.Instance.so.LINGOTTI_COUNTER) * mul;
            PressaScore = ((float)LevelCtrl.Instance.PressaScore / (float)Utils2.Instance.so.LINGOTTI_COUNTER) * mul;
            CesoiaScore = ((float)LevelCtrl.Instance.CesoiaScore / (float)Utils2.Instance.so.LINGOTTI_COUNTER) * mul;

            //if we want multiply everything to display percentage
            string postfix = "%";
            //TotScoreText.text = TotScore.ToString("000");

            //Punteggio-Loader Crogiuolo
            if (LevelCtrl.Instance.CrogiuoloEnded)
            {
                CrogiuoloImg.enabled = false;
                GrogiuoloScoreText.enabled = true;
                //GrogiuoloScoreText.text = CrogiuoloScore.ToString() + postfix;
                GrogiuoloScoreText.text = (CrogiuoloScore == 100.0f) ? "100" : CrogiuoloScore.ToString("00") + postfix;
            } else
            {
                CrogiuoloImg.enabled = true;
                GrogiuoloScoreText.enabled = false;
            }
            //Punteggio-Loader Treno
            if (LevelCtrl.Instance.TrenoEnded)
            {
                TrenoImg.enabled = false;
                TrenoScoreText.enabled = true;
                TrenoScoreText.text = TrenoScore.ToString() + postfix;
                TrenoScoreText.text = (TrenoScore == 100.0f) ? "100" : TrenoScore.ToString("00") + postfix;
            }
            else
            {
                TrenoImg.enabled = true;
                TrenoScoreText.enabled = false;
            }
            //Punteggio-Loader Pressa
            if (LevelCtrl.Instance.PressaEnded)
            {
                PresseImg.enabled = false;
                PressaScoreText.enabled = true;
                //PressaScoreText.text = PressaScore.ToString() + postfix;
                PressaScoreText.text = (PressaScore == 100.0f) ? "100" : PressaScore.ToString("00") + postfix;
            }
            else
            {
                PresseImg.enabled = true;
                PressaScoreText.enabled = false;
            }
            //Punteggio-Loader Cesoia
            if (LevelCtrl.Instance.CesoiaEnded)
            {
                CesoiaImg.enabled = false;
                CesoiaScoreText.enabled = true;
                //CesoiaScoreText.text = CesoiaScore.ToString() + postfix;
                CesoiaScoreText.text = (CesoiaScore == 100.0f) ? "100" : CesoiaScore.ToString("00") + postfix;
            }
            else
            {
                CesoiaImg.enabled = true;
                CesoiaScoreText.enabled = false;
            }
            //GrogiuoloScoreText.text = LevelCtrl.Instance.CrogiuoloEnded ? CrogiuoloScore.ToString("00") + postfix : "WAITING";
            //TrenoScoreText.text = LevelCtrl.Instance.TrenoEnded ? TrenoScore.ToString("000") + postfix : "WAITING";
            //PressaScoreText.text = LevelCtrl.Instance.PressaEnded ? PressaScore.ToString("000") + postfix : "WAITING";
            //CesoiaScoreText.text = LevelCtrl.Instance.CesoiaEnded ? CesoiaScore.ToString("000") + postfix : "WAITING";
            EndTimerText.text = "";
        }
    }

    void updateOnlyTotText()
    {
        TotScore = LevelCtrl.Instance.totScore;
        //TotScoreText.text = TotScore.ToString("00");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleWatcher : MonoBehaviour {
    public float timer = 0;            //Timer will start when the first out of 4 monitors (partially) ends.
    public float IDLETimerVal = 60;    //After these seconds, the game restarts.

    // There will be only one Instance of this class
    public static IdleWatcher Instance { get; private set; }
    public void Awake() { Instance = this; }

    public void OnEnable()
    {
        EventAggregator.partialGameEnded.AddListener(OnPartialEndGame);
    }

    public void OnDisable()
    {
        EventAggregator.partialGameEnded.RemoveListener(OnPartialEndGame);
    }
    void OnPartialEndGame(int id)
    {
        if (timer != 0) return;
        timer = IDLETimerVal;
    }
 	void Start()
    {
        IDLETimerVal = Utils2.Instance.so.END_RESTART_TIMER;
    }
	// Update is called once per frame
	void Update () {
        if (timer == 0) return;
        else{
            timer -= Time.deltaTime;
            if (timer < 0) {
                timer = 0;
                Debug.LogWarning("---- WARNING: IdleWatcher forced game restart (maybe the game loop was stucked at the final gamescore display)");
                //Notify that there is some problem (It is not an Arduino Fault, but in this way Mosaico could reboot the machine)
                StatusCheckerServer2.Instance.cambiastato("2");
                EventAggregator.gameRestart.Invoke();
            }
        }

    }
}

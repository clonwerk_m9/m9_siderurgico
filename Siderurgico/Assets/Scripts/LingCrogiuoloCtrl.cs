﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 CROGIUOLO CHALLENGE
    1. Crogiuolo Ling start Speed is CROGIUOLO_LING_SPEED + RandomRange(-CROGIUOLO_LING_SPEED_MODIFIER, CROGIUOLO_LING_SPEED_MODIFIER)
    2. If Corgiuolo Ling reaches SpeedStopper.boxColliderTrigger before CROGIUOLO_TRIP_DURATION, wait until CROGIUOLO_TRIP_DURATION seconds are passed since it is born
    3. After that, Crogiuolo Ling continues its trip towards the destroyer, with CROGIUOLO_LING_SPEED_AFTER_STOP speed
 */
public class LingCrogiuoloCtrl : LingCtrl {
    float speed;
    private float xPos;
    public float xLimit;
    float timer = 0;
    public bool hitted = false;

    // Use this for initialization
    void Start()
    {
        score = Utils2.Instance.so.CROGIUOLO_EMPTY_MINIMUMSCORE;
        float startSpeed = Utils2.Instance.so.CROGIUOLO_LING_SPEED;
        float rndModifier = Random.Range(-Utils2.Instance.so.CROGIUOLO_LING_SPEED_MODIFIER, Utils2.Instance.so.CROGIUOLO_LING_SPEED_MODIFIER);
        speed = startSpeed + rndModifier;
        //speed = 0.035f - 0.01f;
        //lerpVal range is [0,1] <=> [CROGIUOLO_LING_SPEED-CROGIUOLO_LING_SPEED_MODIFIER, CROGIUOLO_LING_SPEED+CROGIUOLO_LING_SPEED_MODIFIER]
        float lerpVal = Mathf.InverseLerp(  Utils2.Instance.so.CROGIUOLO_LING_SPEED - Utils2.Instance.so.CROGIUOLO_LING_SPEED_MODIFIER,
                                            Utils2.Instance.so.CROGIUOLO_LING_SPEED + Utils2.Instance.so.CROGIUOLO_LING_SPEED_MODIFIER,
                                            speed);
        crogRightRangeMover.Instance.updatePosX(Mathf.Lerp( Utils2.Instance.so.CROGIUOLO_SLOWEST_SPEED_RANGE_POSX,
                                                            Utils2.Instance.so.CROGIUOLO_FASTEST_SPEED_RANGE_POSX, lerpVal));
    }

    //This Ling should reach Stop Place before CROGIUOLO_TRIP_DURATION
    //then wait here until CROGIUOLO_TRIP_DURATION is completed.
    //then its speed will be a standard speed CROGIUOLO_LING_SPEED
    public void stopLing()
    {
        speed = 0;
        StartCoroutine(startLing());
    }

    IEnumerator startLing()
    {
        while (timer < Utils2.Instance.so.CROGIUOLO_TRIP_DURATION)
            yield return 0;
        speed = Utils2.Instance.so.CROGIUOLO_LING_SPEED_AFTER_STOP;
    }

    // Update is called once per frame
    void Update()
    {

        xPos = this.transform.localPosition.x;
        if (xPos < xLimit)
        {
            Destroy(this.gameObject);
            //This event will be catched by the next game, that will initialize the new lingotto with this score value
            EventAggregator.lingDestroyed.Invoke(Utils.LEVA_CROGIUOLO, score);
        }
        float inc = 0.2f;
        if (hitted)
        {
            if (score < Utils2.Instance.so.CROGIUOLO_SCORE_MAX)
                score += inc;
            score = (int)Mathf.Clamp(score, 0, Utils2.Instance.so.CROGIUOLO_SCORE_MAX);
        }

    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        this.transform.Translate(speed, .0f, .0f);
    }
}

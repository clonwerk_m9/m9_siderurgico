﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LingCesoiaView : LingView {
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        scoreText.enabled = ctrl.score > 0;
        ctrl.score = (int)Mathf.Clamp(ctrl.score, 0, 100);
        scoreText.text = ctrl.score.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public enum IntroStates{START, AUTOPLAYGAME, INTRO_VIDEO_0, INTRO_VIDEO_1, LOAD_GAME}

public class IntroMng : MonoBehaviour {

    // There will be only one Instance of this class
    public static IntroMng Instance { get; private set; }
    public void Awake()
    { Instance = this; }

    public IntroStates currState;
	public Image[] fadePanelImgs;
    public VideoPlayer[] vps;
    public GameObject[] AutoplayUIObjs;
    public GameObject[] VideoIntroUIObjs;
    float currFadeAlpha;
	public float fadeInc = 0.3f;
	bool performFadeOut = false;
	bool performFadeIn = false;
    public AudioSource videoAudioSource;
    double videoDuration;
    public double videoCountdown;
    float fadeInStartTime = 0;
    //To avoid black screen in intro scene, we restrart intro scene each vpTimer
    //vpTimer = Utils2.Instance.so.INTRO_TIMEOUT if:
    //  - we enter AUTOPLAYGAME
    //  - we enter INTRO_VIDEO_0
    float vpTimer = 0;

    void Start(){
		vps[0].loopPointReached += videoEndReached;
		currState = IntroStates.START;

        //At the beginning, every monitor is Automatic. automatic == 1
        PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN, 1);
        PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN, 1);
        PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN, 1);
        PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN, 1);
        vpTimer = Utils2.Instance.so.INTRO_TIMEOUT;

        startFadeIn();
    }

	void videoEndReached(UnityEngine.Video.VideoPlayer vp)
	{
        if (currState == IntroStates.INTRO_VIDEO_1)
        {
            startFadeOut();
            switchState(IntroStates.LOAD_GAME);
        }
	}

	public void OnEnable(){
		EventAggregator.leverPressed.AddListener(OnLeverPressed);
	}

	public void OnDisable(){
		//EventAggregator.hoverButtonTriggered.UnSubscribe(OnHoverButtonTriggered);
	}

    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (currState != IntroStates.AUTOPLAYGAME && currState != IntroStates.INTRO_VIDEO_1)
            return;
        if (id == 1)
            PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN, 0);
        else if (id == 2)
            PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN, 0);
        else if (id == 3)
            PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN, 0);
        else if (id == 4)
            PlayerPrefs.SetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN, 0);
        //This is the first player: start to play the intro video
        if (currState == IntroStates.AUTOPLAYGAME)
            switchState(IntroStates.INTRO_VIDEO_0);
    }

    IEnumerator startFadeOut(){
        bool _return;
        do
        {
            _return = false;
            foreach (VideoPlayer vp in vps)
                if (!vp.isPlaying)
                    _return = true;
            if(_return == true)
                yield return 0;
        } while (_return == true);

        currFadeAlpha = 1;
		performFadeOut = true;
		performFadeIn = false;

    }

	void startFadeIn(){
		currFadeAlpha = 0;
		performFadeOut = false;
		performFadeIn = true;
        fadeInStartTime = Time.time;
    }

    IEnumerator playVideo(VideoPlayer vp, string fileURL, bool setAudioSource, bool looping)
    {
        //Disable Play on Awake for both Video and Audio
        vp.playOnAwake = false;
        vp.playOnAwake = false;
        if(setAudioSource)
            videoAudioSource.Pause();

        //We want to play from video clip not from url

        vp.source = VideoSource.Url;
        vp.url = fileURL;
        vp.isLooping = looping;

        //Set Audio Output to AudioSource
        if (setAudioSource)
        {
            vp.audioOutputMode = VideoAudioOutputMode.AudioSource;

            //Assign the Audio from Video to AudioSource to be played
            vp.controlledAudioTrackCount = 1;
            vp.EnableAudioTrack(0, true);
            vp.SetTargetAudioSource(0, videoAudioSource);
        }

        //Set video To Play then prepare Audio to prevent Buffering
        vp.Prepare();

        //Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!vp.isPrepared)
        {
            //Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait
            break;
        }

        //Debug.Log("Done Preparing Video");

        if (setAudioSource)
            //Play Sound
            videoAudioSource.Play();

        //Play Video
        vp.Play();
        if (setAudioSource)
            videoDuration = videoCountdown = vp.frameCount / vp.frameRate;
    }

    void switchState(IntroStates nextState){
		switch (nextState) {
		case IntroStates.AUTOPLAYGAME:
                foreach (var obj in AutoplayUIObjs)
                    obj.SetActive(true);
                foreach (var obj in VideoIntroUIObjs)
                    obj.SetActive(false);
                StartCoroutine(playVideo(vps[0], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.AUTOPLAYVIDEO_URL_0, true, true));
                //We'll use AUTOPLAYVIDEO_URL_N
                StartCoroutine(playVideo(vps[1], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.AUTOPLAYVIDEO_URL_1, false, true));
                StartCoroutine(playVideo(vps[2], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.AUTOPLAYVIDEO_URL_2, false, true));
                StartCoroutine(playVideo(vps[3], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.AUTOPLAYVIDEO_URL_3, false, true));
                StartCoroutine(startFadeOut());
                vpTimer = Utils2.Instance.so.INTRO_TIMEOUT;
                break;
            case IntroStates.INTRO_VIDEO_0:
                //startFadeIn();
                vps[0].Stop();
                vps[1].Stop();
                vps[2].Stop();
                vps[3].Stop();
                vpTimer = Utils2.Instance.so.INTRO_TIMEOUT;
                break;
            case IntroStates.INTRO_VIDEO_1:
                StartCoroutine(playVideo(vps[0], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.INTROVIDEO_URL_0, true, false));
                //We'll use always INTROVIDEO_URL_0
                StartCoroutine(playVideo(vps[1], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.INTROVIDEO_URL_0, false, false));
                StartCoroutine(playVideo(vps[2], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.INTROVIDEO_URL_0, false, false));
                StartCoroutine(playVideo(vps[3], "C:\\Resources\\Siderurgico\\" + Utils2.Instance.so.INTROVIDEO_URL_0, false, false));
                foreach (var obj in AutoplayUIObjs)
                    obj.SetActive(false);
                foreach (var obj in VideoIntroUIObjs)
                    obj.SetActive(true);
                //StartCoroutine(startFadeOut());
                break;
            case IntroStates.LOAD_GAME:
                vps[0].Stop();
                vps[1].Stop();
                vps[2].Stop();
                vps[3].Stop();
                startFadeIn();
			break;
		}
		currState = nextState;
	}

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
    void Update () {
        if(vpTimer > 0)
        {
            vpTimer -= Time.deltaTime;
            if(vpTimer < 0)
                SceneManager.LoadScene(0);
        }
		if (currState == IntroStates.START && Time.time > 2) {
			switchState (IntroStates.AUTOPLAYGAME);
		}
		if (performFadeIn) {
			currFadeAlpha += fadeInc * Time.deltaTime;
            foreach (var currFadePanel in fadePanelImgs)
                currFadePanel.color = new Color(currFadePanel.color.r, currFadePanel.color.g, currFadePanel.color.b, currFadeAlpha);
            if (currFadeAlpha > 1)
                performFadeIn = false;
        }
		else if (performFadeOut) {
			currFadeAlpha -= fadeInc * Time.deltaTime;
            foreach (var currFadePanel in fadePanelImgs)
                currFadePanel.color = new Color(currFadePanel.color.r, currFadePanel.color.g, currFadePanel.color.b, currFadeAlpha);
                if(currFadeAlpha <=0)
                    performFadeOut = false;
    	}

		if (currState == IntroStates.INTRO_VIDEO_0) {
            //if (performFadeIn == false && performFadeOut == false && currFadeAlpha >= 1) {
            if (performFadeIn == false && performFadeOut == false)
            {
                switchState(IntroStates.INTRO_VIDEO_1);
                startFadeOut ();
            }
		}
		if (currState == IntroStates.LOAD_GAME) {
			if (performFadeIn == false && performFadeOut == false)
				SceneManager.LoadScene (2);
		}
        if (currState == IntroStates.INTRO_VIDEO_1) {
            videoCountdown = videoDuration - vps[0].time;
        }

        //DEBUG
        if (Input.GetKeyDown(KeyCode.S))
            videoEndReached(null);
        if ((performFadeIn || performFadeOut) && (Time.time - fadeInStartTime > 5))
        {
            performFadeIn = performFadeOut = false;

            foreach (var currFadePanel in fadePanelImgs)
                currFadePanel.color = new Color(currFadePanel.color.r, currFadePanel.color.g, currFadePanel.color.b, 0);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CesoiaDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision obj)
	{
        LingCesoiaCtrl ctrl = obj.gameObject.GetComponent<LingCesoiaCtrl>();
        if (ctrl == null) return;
        float score = ctrl.score;
        Destroy (obj.gameObject);
        //This event will be catched by the next game, that will initialize the new lingotto with this score value
        EventAggregator.lingDestroyed.Invoke(Utils.LEVA_CESOIA, score);
    }
}

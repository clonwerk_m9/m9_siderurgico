﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LingPressaView : LingView {
	void Start () {
	}
	
	void Update ()
    {
        scoreText.enabled = ctrl.score > 0;
        ctrl.score = (int)Mathf.Clamp(ctrl.score, 0, 100);
        scoreText.text = ctrl.score.ToString();
    }
}

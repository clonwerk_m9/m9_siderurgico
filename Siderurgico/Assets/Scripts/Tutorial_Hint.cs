﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum _tutorialType { CROGIUOLO, TRENO, PRESSA, CESOIA };
public class Tutorial_Hint : MonoBehaviour {

    public _tutorialType tutorialType;
    public GameObject RightTimeFeedbackObj;
    GameObject child; //child root for tutorial panel

    public void OnEnable()
    {
        //EventAggregator.lingDestroyed.AddListener(OnLingDestroyed);
        EventAggregator.leverPressedAtRightTime.AddListener(OnLeverPressedAtRightTime);
        EventAggregator.setActiveAutomatic.AddListener(OnSetActiveAutomatic);
    }

    public void OnDisable()
    {
        //EventAggregator.lingDestroyed.RemoveListener(OnLingDestroyed);
        EventAggregator.leverPressedAtRightTime.RemoveListener(OnLeverPressedAtRightTime);
        EventAggregator.setActiveAutomatic.RemoveListener(OnSetActiveAutomatic);
    }
    private void Awake()
    {
        child = transform.GetChild(0).gameObject;
    }
    private void Start()
    {
        /*
        if (tutorialType == _tutorialType.CROGIUOLO)
            child.SetActive(PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN) == 1);
        else if (tutorialType == _tutorialType.TRENO)
            child.SetActive(PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN) == 1);
        else if (tutorialType == _tutorialType.PRESSA)
            child.SetActive(PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN) == 1);
        else if (tutorialType == _tutorialType.CESOIA)
            child.SetActive(PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 1);
            */
    }

    void OnSetActiveAutomatic(int id, bool isActive)
    {
		//Tutorial is active when this monitor is NOT automatic 
        if (id == Utils.LEVA_CROGIUOLO && tutorialType == _tutorialType.CROGIUOLO)
            child.SetActive(!isActive);
        else if (id == Utils.LEVA_TRENO && tutorialType == _tutorialType.TRENO)
            child.SetActive(!isActive);
        else if (id == Utils.LEVA_PRESSA && tutorialType == _tutorialType.PRESSA)
            child.SetActive(!isActive);
        else if (id == Utils.LEVA_CESOIA && tutorialType == _tutorialType.CESOIA)
            child.SetActive(!isActive);
    }
	/*
    void OnLingDestroyed(int id, float score)
    {
        //For Crogiuolo, we'll check if the player used the lever at right time after the first lingotto is outside the stage
        if (tutorialType == _tutorialType.CROGIUOLO && id == Utils.LEVA_CROGIUOLO && score > Utils2.Instance.so.CROGIUOLO_EMPTY_MINIMUMSCORE)
        {
            child.SetActive(false);
            EventAggregator.leverPressedAtRightTime.Invoke(Utils.LEVA_CROGIUOLO);
        }
    }
    */
    void OnLeverPressedAtRightTime(int id)
    {
        //For Treno, PRessa and Cesoia, we'll know if the player used the lever at right time if this event is fired
        if (tutorialType == _tutorialType.TRENO && id == Utils.LEVA_TRENO) {
            child.SetActive(false);
            //TODO: Activate RIGHT TIME LEVER PRESSED FEEDBACK
            RightTimeFeedbackObj.SetActive(true);
        }
        else if (tutorialType == _tutorialType.PRESSA && id == Utils.LEVA_PRESSA) {
            child.SetActive(false);
            //TODO: Activate RIGHT TIME LEVER PRESSED FEEDBACK
            RightTimeFeedbackObj.SetActive(true);
        }
        else if (tutorialType == _tutorialType.CESOIA && id == Utils.LEVA_CESOIA) {
            child.SetActive(false);
            //TODO: Activate RIGHT TIME LEVER PRESSED FEEDBACK
            RightTimeFeedbackObj.SetActive(true);
        }
        else if (tutorialType == _tutorialType.CROGIUOLO && id == Utils.LEVA_CROGIUOLO)
        {
			child.SetActive(false);
            //TODO: Activate RIGHT TIME LEVER PRESSED FEEDBACK
            RightTimeFeedbackObj.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LingPressaCtrl : LingCtrl {
    Rigidbody rb;
    public Transform LeftRange;
    public Transform RightRange;
    public Transform LeftRightRange;
    public Transform RightRightRange;

    bool pressed = false;

    public void OnEnable()
    {
        EventAggregator.leverPressed.AddListener(OnLeverPressed);
        EventAggregator.lingCreated.AddListener(OnLingCreated);
    }

    public void OnDisable()
    {
        EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
        EventAggregator.lingCreated.RemoveListener(OnLingCreated);
    }

    void OnLingCreated(int id)
    {
        //if (id == Utils.LEVA_PRESSA)
        //    pressed = false;
    }

    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (id != Utils.LEVA_PRESSA)
            return;
        if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN) == 1)
            return;
        if (pressed)
            return;

        //Check if we can press the Lever
        if (transform.position.x > LeftRange.position.x && transform.position.x < RightRange.position.x)
        {
            pressed = true;

            if (transform.position.x < LeftRightRange.position.x || transform.position.x > RightRightRange.position.x)
            {
                //score /= 2;
                EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_PRESSA);
            }
            else
                EventAggregator.leverPressedAtRightTime.Invoke(Utils.LEVA_PRESSA);
            //rb.isKinematic = false;
        }

        /*
        else
        {
            EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_PRESSA);
        }
        */
    }

    // Use this for initialization
    void Start()
    {

        LeftRange = GameObject.Find("Pressa_LeftRange").transform;
        RightRange = GameObject.Find("Pressa_RightRange").transform;
        LeftRightRange = GameObject.Find("Pressa_LeftRightRange").transform;
        RightRightRange = GameObject.Find("Pressa_RightRightRange").transform;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        //If we missed the lever, half the score
        if (transform.position.x > RightRange.position.x && !pressed)
        {
            //score /= 2;
            pressed = true;
            EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_PRESSA);
        }
    }
}

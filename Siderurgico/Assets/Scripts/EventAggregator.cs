using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;

public class EventAggregator
{
    public class _leverPressed : UnityEvent<int, bool> { public string id = "levelPressed"; }   //ID Lever, Is human action
    public class _lingDestroyed : UnityEvent<int, float> { public string id = "lingDestroyed"; } //Every time a lingotto is destroyed (Arrives at the end of the stage) Eg. (Utils.LEVA_CROGIUOLO, score)
	public class _lingCreated : UnityEvent<int> { public string id = "lingCreated"; }
    public class _trenoPinzeTake : UnityEvent { public string id = "trenoPinzeTake"; } //Treno Pinze are about to take the new lingotto
    public class _trenoPinzeDrop : UnityEvent { public string id = "trenoPinzeDrop"; } //Treno Pinze are about to fall on binary 
	public class _leverPressedWrongTime : UnityEvent<int> { public string id = "leverPressedWrongTime"; } //Player press the lever outside the allowed range (no effect)
	public class _addPartialPoint : UnityEvent<float> { public string id = "addPartialPoint"; } //Player press the lever outside the allowed range (no effect)
    public class _setActiveAutomatic : UnityEvent<int, bool> { public string id = "setActiveAutomatic"; } //NOT USED
	public class _leverPressedAtRightTime : UnityEvent<int> { public string id = "leverPressedAtRightTime"; } //This is fired every time a lever is pressed and the score is not cutted by half
	public class _missedCesoia : UnityEvent<GameObject> { public string id = "missedCesoia"; }
	public class _endGame : UnityEvent { public string id = "endGame"; }
    public class _gameRestart : UnityEvent { public string id = "gameRestart"; }
    public class _partialGameEnded : UnityEvent<int> { public string id = "partialGameEnded"; } //Fired when the current game processed the max number of stampi
    public class _updateFinalScorePanels : UnityEvent { public string id = "updateFinalScorePanels"; } 

    //TODO
    public class _beginCrogiuoloPoint : UnityEvent { public string id = "beginCrogiuoloPoint"; } //First time Crogiuolo particles collide with stampo
    public class _endCrogiuoloPoint : UnityEvent { public string id = "beginCrogiuoloPoint"; } //Crogiuolo particles stop to collide with stampo
    public class _trenoStart : UnityEvent { public string id = "trenoStart"; } //TrenoStart animation
    public class _trenoLingottoHitFloor : UnityEvent { public string id = "trenoLingottoHitFloor"; } //Treno lingotto ha toccato terra
    public class _autoPress : UnityEvent { public string id = "autoPress"; } //Automatic press triggered
    public class _cesoiaIronHitFloor : UnityEvent { public string id = "cesoiaIronHitFloor"; } //After cesoia lever, two iron parts should hit floor
    //---

    public class _tutorialOn : UnityEvent { }

    public static _leverPressed leverPressed    = new _leverPressed();
	public static _lingDestroyed lingDestroyed 	= new _lingDestroyed();
	public static _lingCreated lingCreated 	= new _lingCreated();
    public static _trenoPinzeTake trenoPinzeTake 	= new _trenoPinzeTake();
    public static _trenoPinzeDrop trenoPinzeDrop 	= new _trenoPinzeDrop();
    public static _leverPressedWrongTime leverPressedWrongTime = new _leverPressedWrongTime();
    public static _addPartialPoint addPartialPoint 	= new _addPartialPoint();
	public static _tutorialOn tutorialOn        	= new _tutorialOn();
    public static _setActiveAutomatic setActiveAutomatic = new _setActiveAutomatic();
	public static _leverPressedAtRightTime leverPressedAtRightTime = new _leverPressedAtRightTime();
	public static _missedCesoia missedCesoia = new _missedCesoia();

    public static _beginCrogiuoloPoint beginCrogiuoloPoint = new _beginCrogiuoloPoint();
    public static _endCrogiuoloPoint endCrogiuoloPoint = new _endCrogiuoloPoint();
    public static _trenoStart trenoStart = new _trenoStart();
    public static _trenoLingottoHitFloor trenoLingottoHitFloor = new _trenoLingottoHitFloor();
    public static _autoPress autoPress = new _autoPress();
	public static _cesoiaIronHitFloor cesoiaIronHitFloor = new _cesoiaIronHitFloor();
    public static _gameRestart gameRestart = new _gameRestart();
    public static _partialGameEnded partialGameEnded = new _partialGameEnded();
    public static _updateFinalScorePanels updateFinalScorePanels = new _updateFinalScorePanels();

    public static _endGame endGame = new _endGame();
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Utils_Data", menuName = "M9_Siderurgico", order = 1)]
public class SO_Utils : ScriptableObject
{
    //VIDEO URLS
    public  string AUTOPLAYVIDEO_URL_0 = "AUTOPLAYVIDEO_0";
    public  string AUTOPLAYVIDEO_URL_1 = "AUTOPLAYVIDEO_0";
    public  string AUTOPLAYVIDEO_URL_2 = "AUTOPLAYVIDEO_0";
    public  string AUTOPLAYVIDEO_URL_3 = "AUTOPLAYVIDEO_0";
    public string INTROVIDEO_URL_0 = "INTROVIDEO_0";
    public float INTRO_TIMEOUT = 70;


    //PlayerPrefs keys ----------------------
    public string PPKEY_LANGUAGE = "LANGUAGE";   //string
    public  string PPKEY_USERNAME = "USERNAME";       //string
    public  string PPKEY_SCORE_TOT = "SCORE_TOT";     //int
    public  string PPKEY_SCORE_CARDATURA = "PPKEY_SCORE_CARDATURA";     //int
    public  string PPKEY_SCORE_FILATURA = "PPKEY_SCORE_FILATURA";     //int
    public  string PPKEY_SCORE_TESSITURA = "PPKEY_SCORE_TESSITURA";     //int
    public string PPKEY_LEVEL = "LEVEL";     //int

    public string PPKEY_PLAYER1_HUMAN = "PPKEY_PLAYER1_HUMAN";     //int
    public string PPKEY_PLAYER2_HUMAN = "PPKEY_PLAYER2_HUMAN";     //int
    public string PPKEY_PLAYER3_HUMAN = "PPKEY_PLAYER3_HUMAN";     //int
    public string PPKEY_PLAYER4_HUMAN = "PPKEY_PLAYER4_HUMAN";     //int

    //PlayerPrefs values
    public string PPVAL_LANGUAGE_ITA = "ITA";
    public  string PPVAL_LANGUAGE_ENG = "ENG";

    // --- LEVE
    public const int LEVA_CROGIUOLO = 1;
    public const int LEVA_TRENO = 2;
    public const int LEVA_PRESSA = 3;
    public const int LEVA_CESOIA = 4;
    public string oscInputID_crogiuolo = "/lever_crogiuolo";
    public string oscInputID_treno = "/lever_treno";
    public string oscInputID_pressa = "/lever_pressa";
    public string oscInputID_cesoia = "/lever_cesoia";
    public string SENSORS_INPUT_IP = "127.0.0.1";
    public int SENSORS_INPUT_PORT = 12000;


    // --- CROGIUOLO
    public float CROGIUOLO_MOVEFORWARD_TIME = 1.0f;     //How long does the crogiuolo takes to move forward?
    public float CROGIUOLO_DRIPPING_TIME = 1.0f;        //How long does the crogiuolo stay in forward position?
    public float CROGIUOLO_MOVEBACK_TIME = 1.0f;        //How long does the crogiuolo takes to move backward?
    public float CROGIUOLO_SCORE_INC_ONCOLLISION = 1.0f;     //If Lava collides with Stampo, this is the increment
    public float CROGIUOLO_SCORE_MAX = 100.0f;          //If Lava collides with Stampo, this is the increment
    public float CROGIUOLO_TRASL_MAX = 0.00f;       //If Lava collides with Stampo, this is the translation increment of the Ghisa level
    public float CROGIUOLO_TRASL_MIN = -0.415f;     //If Lava collides with Stampo, this is the translation increment of the Ghisa level
    public float CROGIUOLO_SPAWN_TIME = 10.0f;         //Each X seconds the Crogiuolo spawn a Stampo
    public float CROGIUOLO_EMPTY_MINIMUMSCORE = 8.0f;  //IfCrogiuolo score is 0.. Treno will start with this score
    public float CROGIUOLO_LING_SPEED = 0.035f;  //start velocity of crogiuolo stampo. This standard velocity will be modified by a random value.
    public float CROGIUOLO_TRIP_DURATION = 6f;  //How long doeas it take for the Crogiuolo ling from start to Stop place? From stop place on crogiuolo ling will have standard speed CROGIUOLO_LING_SPEED
    public float CROGIUOLO_LING_SPEED_AFTER_STOP = 0.035f;   //After Stop place the ling will have this speed
    public float CROGIUOLO_LING_SPEED_MODIFIER = 0.02f;   //How wide is the ling speed random range?
    public float CROGIUOLO_SLOWEST_SPEED_RANGE_POSX = 0f;
    public float CROGIUOLO_FASTEST_SPEED_RANGE_POSX = 0f;

    // --- CESOIA
    public float CESOIA_LING_SPEED = 0.1f;   //start velocity of cesoia ling. This standard velocity will be modified by a random value.
    public float CESOIA_LING_SPEED_MODIFIER = 0.1f;   //How wide is the ling speed random range?

    //GENERAL
    public int LINGOTTI_COUNTER = 10; //After N lingotti, game ENDS
    public int END_DISPLAYSCORE_TIMER = 2; //After last biletta, display the finalScore panel on the Cesoia monitor after END_DISPLAYSCORE_TIMER seconds
    public int END_RESTART_TIMER = 10; //After last biletta, the game restarts after END_RESTART_TIMER sceonds
}

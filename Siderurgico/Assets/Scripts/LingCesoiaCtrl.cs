﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 CESOIA
    1. Cesoia Ling (Billetta) start Speed is CESOIA_LING_SPEED + RandomRange(CESOIA_LING_SPEED_MODIFIER, CESOIA_LING_SPEED_MODIFIER*4)
 */
public class LingCesoiaCtrl : LingCtrl {
    float speed;

    Rigidbody rb;
    public Transform LeftRange;
    public Transform RightRange;
    public Transform LeftRightRange;
	public Transform RightRightRange;
	public float powerIfNotPressed;
    public bool pressed = false;

    public void OnEnable()
    {
        EventAggregator.leverPressed.AddListener(OnLeverPressed);
    }

    public void OnDisable()
    {
        EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
    }

    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (id != Utils.LEVA_CESOIA)
            return;
        if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 1)
            return;
        //Debug.Log("OnLeverPressed CESOIA");


        if (transform.position.x > LeftRightRange.position.x && transform.position.x < RightRightRange.position.x &&
            !pressed && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 1)
        {
            //Automatic cesoia trigger is invoked only if this monitor is automatic, and is triggered by AutomateClient.cs!
            pressed = true;
            EventAggregator.leverPressedAtRightTime.Invoke(Utils.LEVA_CESOIA);
            CesoiaCtrl.Instance.bilettePassed++;
        }

        //Check if we can press the Lever
        else if (transform.position.x > LeftRange.position.x && transform.position.x < RightRange.position.x
            && !pressed && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 0)
        {
            pressed = true;
            if (transform.position.x < LeftRightRange.position.x || transform.position.x > RightRightRange.position.x)
            {
                //score /= 2; //We no longer care about score 
                EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_CESOIA);
                CesoiaCtrl.Instance.bilettePassed++;
            }
            else
            {
                EventAggregator.leverPressedAtRightTime.Invoke(Utils.LEVA_CESOIA);
                CesoiaCtrl.Instance.bilettePassed++;
                //EventAggregator.addPartialPoint.Invoke(score); //We ain't care about score 
            }
            //rb.isKinematic = false;
        }
        //[2/25.04.29]
        else if (transform.position.x > LeftRange.position.x)
        {
            EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_CESOIA);
        }
        //EventAggregator.addPartialPoint.Invoke(score);//We ain't care about score 
    }
    // Use this for initialization
    void Start()
    {
        LeftRange = GameObject.Find("Cesoia_LeftRange").transform;
        RightRange = GameObject.Find("Cesoia_RightRange").transform;
        LeftRightRange = GameObject.Find("Cesoia_LeftRightRange").transform;
        RightRightRange = GameObject.Find("Cesoia_RightRightRange").transform;
        rb = GetComponent<Rigidbody>();
        speed = Utils2.Instance.so.CESOIA_LING_SPEED;
        float rndModifier = Random.Range(Utils2.Instance.so.CESOIA_LING_SPEED_MODIFIER, Utils2.Instance.so.CESOIA_LING_SPEED_MODIFIER*4);
        speed += rndModifier;
    }

    // Update is called once per frame
    void Update()
    {
		if (LevelCtrl.Instance.gameEnded)
			return;

        if (transform.position.x > RightRange.position.x
            && !pressed && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 0)
        {
            EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_CESOIA);
            CesoiaCtrl.Instance.bilettePassed++;
            //[3/25.04.29]
            Destroy(this.gameObject);
            EventAggregator.missedCesoia.Invoke(this.gameObject);
        }

    }

    private void FixedUpdate()
    {
//		if(performAutoTranslate)
	        transform.Translate(0, 0, speed);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayEndPanelCtrl : MonoBehaviour {

    //NB: THIS CAN'T BE A SINGLETON: we have 4 of them

    public LingottoCounter LC_Crogiuolo, LC_Treno, LC_Pressa, LC_Cesoia;    //From here we'll read how many partial Lingots we have at each partialGameEnded event
    public GameObject EndGORoot;        //This obj will be activated to show end score panel
    public LingottoCounter counterCtrl;
    public Transform LingotsRoot_Crogiuolo, LingotsRoot_Treno, LingotsRoot_Pressa, LingotsRoot_Cesoia;
    //public GameObject Waiting_Crogiuolo, Waiting_Treno, Waiting_Pressa, Waiting_Cesoia; //These objs will be visible until the associated part is ended
    public GameObject SingleLingot;     //This is the single lingot prefab, that will be displaied as empty or full depending on the partial scores of each monitor
    GameObject[] FullLingots_Crogiuolo, FullLingots_Treno, FullLingots_Pressa, FullLingots_Cesoia; //Each of this array contains full lingots to disaply or hide according to LC_Crogiuolo/Pressa/etc.FullLingots[]
    public Text totScoreText;
	public Transform MonitorIndicatorRoot; //Under this root obj there will be only one active image, indicating which one of the monitor belongs to the final score panel

    bool active = false;
    //public bool[] isAutoDisplay = new bool[4]; //isAutoDisplay[0] == true if Crogiuolo is an Automatic station

    void Start()
    {
		EndGORoot.SetActive (false);
        FullLingots_Crogiuolo = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];
        FullLingots_Treno = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];
        FullLingots_Pressa = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];
        FullLingots_Cesoia = new GameObject[Utils2.Instance.so.LINGOTTI_COUNTER];
    }

    public void OnEnable()
	{
        EventAggregator.endGame.AddListener(OnEndGame);
        EventAggregator.partialGameEnded.AddListener(OnPartialEndGame);
        EventAggregator.updateFinalScorePanels.AddListener(OnLeverPressedAtRightTime);
    }

    public void OnDisable()
	{
		EventAggregator.endGame.RemoveListener(OnEndGame);
        EventAggregator.partialGameEnded.RemoveListener(OnPartialEndGame);
        EventAggregator.updateFinalScorePanels.AddListener(OnLeverPressedAtRightTime);
    }

    void OnEndGame()
    {
        EndGORoot.SetActive(true);
    }

    void OnLeverPressedAtRightTime()
    {
        if(active)
            UpdateLingots();
    }

	void activateMonitorIndicator(){
		//Activate the correct monitor indicator
		for (int i = 0; i < 4; i++) {
			GameObject currIndicator = MonitorIndicatorRoot.GetChild (i).gameObject;
			if ((i == 0 && counterCtrl.counterType == _tutorialType.CROGIUOLO) ||
				(i == 1 && counterCtrl.counterType == _tutorialType.TRENO) ||
				(i == 2 && counterCtrl.counterType == _tutorialType.PRESSA) ||
				(i == 3 && counterCtrl.counterType == _tutorialType.CESOIA))
				currIndicator.SetActive (true);
			else
				currIndicator.SetActive (false);
		}
	}

    void OnPartialEndGame(int id)
    {
        if ((id == Utils.LEVA_CROGIUOLO && counterCtrl.counterType == _tutorialType.CROGIUOLO) ||
            (id == Utils.LEVA_TRENO && counterCtrl.counterType == _tutorialType.TRENO) ||
            (id == Utils.LEVA_PRESSA && counterCtrl.counterType == _tutorialType.PRESSA) ||
            (id == Utils.LEVA_CESOIA && counterCtrl.counterType == _tutorialType.CESOIA))
        {
            if (active)
                return;
            active = true;
            EndGORoot.SetActive(true);
			activateMonitorIndicator ();

            //The Score panel is displayed for the first time on this monitor at this moment.
            //Instantiate all Empty lingots for each monitor. We'll fullfil them in the next step
            for (int i = 0; i < Utils2.Instance.so.LINGOTTI_COUNTER; i++)
            {
                GameObject currLingot = null;
                if (!LevelCtrl.Instance.isAutoDisplay[0])
                {
                    currLingot = Instantiate(SingleLingot, LingotsRoot_Crogiuolo);
                    FullLingots_Crogiuolo[i] = currLingot.transform.GetChild(0).gameObject;
                    currLingot.transform.GetChild(0).gameObject.SetActive(false);
                    currLingot.transform.GetChild(1).gameObject.SetActive(false);
                }
                if (!LevelCtrl.Instance.isAutoDisplay[1])
                {
                    currLingot = Instantiate(SingleLingot, LingotsRoot_Treno);
                    FullLingots_Treno[i] = currLingot.transform.GetChild(0).gameObject;
                    currLingot.transform.GetChild(0).gameObject.SetActive(false);
                    currLingot.transform.GetChild(1).gameObject.SetActive(false);
                }
                if (!LevelCtrl.Instance.isAutoDisplay[2])
                {
                    currLingot = Instantiate(SingleLingot, LingotsRoot_Pressa);
                    FullLingots_Pressa[i] = currLingot.transform.GetChild(0).gameObject;
                    currLingot.transform.GetChild(0).gameObject.SetActive(false);
                    currLingot.transform.GetChild(1).gameObject.SetActive(false);
                }
                if (!LevelCtrl.Instance.isAutoDisplay[3])
                {
                    currLingot = Instantiate(SingleLingot, LingotsRoot_Cesoia);
                    FullLingots_Cesoia[i] = currLingot.transform.GetChild(0).gameObject;
                    currLingot.transform.GetChild(0).gameObject.SetActive(false);
                    currLingot.transform.GetChild(1).gameObject.SetActive(false);
                }
            }
            UpdateLingots();
        }
    }

    void UpdateLingots() {
        float totScore = 0;
        //Read Lingots states (Full/Empty)
        for (int i = 0; i < Utils2.Instance.so.LINGOTTI_COUNTER; i++)
        {
            if (LC_Crogiuolo.FullLingots[i].activeSelf && !LevelCtrl.Instance.isAutoDisplay[0])
            {
                FullLingots_Crogiuolo[i].SetActive(true);
                totScore++;
            }
            if (LC_Treno.FullLingots[i].activeSelf && !LevelCtrl.Instance.isAutoDisplay[1])
            {
                FullLingots_Treno[i].SetActive(true);
                totScore++;
            }
            if (LC_Pressa.FullLingots[i].activeSelf && !LevelCtrl.Instance.isAutoDisplay[2])
            {
                FullLingots_Pressa[i].SetActive(true);
                totScore++;
            }
            if (LC_Cesoia.FullLingots[i].activeSelf && !LevelCtrl.Instance.isAutoDisplay[3])
            {
                FullLingots_Cesoia[i].SetActive(true);
                totScore++;
            }
        }
        int humansInGame = 0; //humansInGame == 2 if 2 out of 4 displays are controlled by humans
        for (int i = 0; i < 4; i++)
            if (!LevelCtrl.Instance.isAutoDisplay[i])
                humansInGame++;
        //Score average is calculated only for humans
        float ratio = (totScore / (Utils2.Instance.so.LINGOTTI_COUNTER * humansInGame));
        float percentage = ratio * 100f;
        if(percentage == 100)
            totScoreText.text = ((int)percentage).ToString("D3") + "%";
        else
            totScoreText.text = ((int)percentage).ToString("D2") + "%";
    }
}

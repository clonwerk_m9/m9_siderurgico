﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class updateLocText : MonoBehaviour {
	public string HANDLE;

	void Start () {
	}
	void Update(){
		if(GetComponent<Text> ().text == HANDLE){
			GetComponent<Text> ().text = LocalizationCtrl.Instance.getLocText (HANDLE);
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputKeybDebug : MonoBehaviour {
    public KeyCode level01_CrogiuoloKey;
    public KeyCode level02_TrenoloKey;
    public KeyCode level03_PressaloKey;
    public KeyCode level04_CesoiaKey;

    public InputMng inputMng;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(level01_CrogiuoloKey))
            inputMng.Leva01_Crogiuolo = true;
        else if (Input.GetKeyDown(level02_TrenoloKey))
            inputMng.Leva02_Treno = true;
        else if (Input.GetKeyDown(level03_PressaloKey))
            inputMng.Leva03_Pressa = true;
        else if (Input.GetKeyDown(level04_CesoiaKey))
            inputMng.Leva04_Cesoia = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum _lingTrenoState { SPAWNED, TAKEN, DROPPED }
public class LingTrenoCtrl : LingCtrl {
    
    public _lingTrenoState state;
    public Transform pinzaParent;
    Rigidbody rb;
    public Transform TopRange;
    public Transform LowRange;
    public Transform TopRightRange;
    public Transform LowRightRange;
    bool floorHitted = false;

    public void OnEnable()
    {
        EventAggregator.trenoPinzeTake.AddListener(OnTrenoPinzeTake);
        EventAggregator.trenoPinzeDrop.AddListener(OnTrenoPinzeDrop);
    }

    public void OnDisable()
    {
        EventAggregator.trenoPinzeTake.RemoveListener(OnTrenoPinzeTake);
        EventAggregator.trenoPinzeDrop.RemoveListener(OnTrenoPinzeDrop);
    }

    void OnTrenoPinzeTake()
    {
        if(state == _lingTrenoState.SPAWNED)
        {
            state = _lingTrenoState.TAKEN;
            transform.parent = pinzaParent;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

        }
    }

    //Called by OnLeverPressed(TRENO)
    void OnTrenoPinzeDrop()
    {
        if (state != _lingTrenoState.TAKEN)
            return;
        //Check if we can press the Lever
        if (transform.position.z > LowRange.position.z && transform.position.z < TopRange.position.z)
        {
            if (transform.position.z < LowRightRange.position.z || transform.position.z > TopRightRange.position.z)
            {
                //score /= 2;
                EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_TRENO);
            }
            else
                EventAggregator.leverPressedAtRightTime.Invoke(Utils.LEVA_TRENO);
            state = _lingTrenoState.DROPPED;
            rb.isKinematic = false;
            transform.parent = null;
        }
        else
        {
            //The user pressed the lever outside the red range: DO NOTHING
            ;// EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_TRENO);
        }
    }

    // Use this for initialization
    void Start()
    {
        state = _lingTrenoState.SPAWNED;
        pinzaParent = GameObject.Find("PINZA_PARENT").transform;
        TopRange = GameObject.Find("Treno_TopRange").transform;
        LowRange = GameObject.Find("Treno_LowRange").transform;
        TopRightRange = GameObject.Find("Treno_TopRightRange").transform;
        LowRightRange = GameObject.Find("Treno_LowRightRange").transform;
        rb = GetComponent<Rigidbody>();
        floorHitted = false;
    }

    void OnTriggerEnter(Collider c)
    {
        if(c.tag == "trenoConveyor" && !floorHitted)
        {
            floorHitted = true;
            EventAggregator.trenoLingottoHitFloor.Invoke();
        }
    }
}

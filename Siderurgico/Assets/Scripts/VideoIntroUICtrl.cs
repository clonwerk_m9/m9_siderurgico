﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum _playerID { p1, p2, p3, p4}
public class VideoIntroUICtrl : MonoBehaviour {
    public GameObject isHumanObjs;  //root UI obj. Under this there are all UI objs that must be active if this Monitor is an Automatic one (nobody pressed the lever yet) 
    public GameObject isAutoObjs;   //root UI obj. Under this there are all UI objs that must be active if this Monitor is associated to a human player(someone pressed the lever during the intro video)
    public _playerID playerID; //1,2,3,4

	void Update () {
		if( (playerID == _playerID.p1 && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN) == 0) ||
            (playerID == _playerID.p2 && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN) == 0) ||
            (playerID == _playerID.p3 && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN) == 0) ||
            (playerID == _playerID.p4 && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 0))
        {
            isHumanObjs.SetActive(true);
            isAutoObjs.SetActive(false);
        }
        else
        {
            isHumanObjs.SetActive(false);
            isAutoObjs.SetActive(true);
        }
    }
}

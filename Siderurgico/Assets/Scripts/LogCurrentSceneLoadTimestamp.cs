﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogCurrentSceneLoadTimestamp : MonoBehaviour {
	void Start () {
        Debug.Log("---- SCENE LOADED: "+ SceneManager.GetActiveScene().name + " at Time " + System.DateTime.UtcNow.ToString("HH:mm:ss.fff: "));
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum MotorState { OFF, ON, BLOCKED }

[Serializable]
public class Utils : MonoBehaviour
{
    // There will be only one Instance of this class
    public static Utils Instance { get; private set; }
    public void Awake() { Instance = this; }

    //VIDEO URLS
    public static string AUTOPLAYVIDEO_URL_0 = "AUTOPLAYVIDEO_0";
    public static string AUTOPLAYVIDEO_URL_1 = "AUTOPLAYVIDEO_1";
    public static string AUTOPLAYVIDEO_URL_2 = "AUTOPLAYVIDEO_2";
    public static string AUTOPLAYVIDEO_URL_3 = "AUTOPLAYVIDEO_3";
    public static string INTROVIDEO_URL_0 = "INTROVIDEO_0";


    //PlayerPrefs keys ----------------------
    public static string PPKEY_LANGUAGE = "LANGUAGE";   //string
    public static string PPKEY_USERNAME = "USERNAME";       //string
    public static string PPKEY_SCORE_TOT = "SCORE_TOT";     //int
    public static string PPKEY_SCORE_CARDATURA = "PPKEY_SCORE_CARDATURA";     //int
    public static string PPKEY_SCORE_FILATURA = "PPKEY_SCORE_FILATURA";     //int
    public static string PPKEY_SCORE_TESSITURA = "PPKEY_SCORE_TESSITURA";     //int
    public static string PPKEY_LEVEL = "LEVEL";     //int

    //PlayerPrefs values
    public static string PPVAL_LANGUAGE_ITA = "ITA";
    public static string PPVAL_LANGUAGE_ENG = "ENG";
    public static string PPVAL_LEVEL_CARDATURA = "CARDATURA";
    public static string PPVAL_LEVEL_FILATURA = "FILATURA";
    public static string PPVAL_LEVEL_TESSITURA = "TESSITURA";

    // --- LEVE
    public const int LEVA_CROGIUOLO = 1;
    public const int LEVA_TRENO     = 2;
    public const int LEVA_PRESSA    = 3;
    public const int LEVA_CESOIA    = 4;
    public string oscInputID_crogiuolo  = "/lever_crogiuolo";
    public string oscInputID_treno      = "/lever_treno";
    public string oscInputID_pressa     = "/lever_pressa";
    public string oscInputID_cesoia     = "/lever_cesoia";
    public string SENSORS_INPUT_IP = "127.0.0.1";
    public int SENSORS_INPUT_PORT = 12000;


    // --- CROGIUOLO
    public float CROGIUOLO_MOVEFORWARD_TIME = 1.0f;     //How long does the crogiuolo takes to move forward?
    public float CROGIUOLO_DRIPPING_TIME = 1.0f;        //How long does the crogiuolo stay in forward position?
    public float CROGIUOLO_MOVEBACK_TIME = 1.0f;        //How long does the crogiuolo takes to move backward?
    public float CROGIUOLO_SCORE_INC_ONCOLLISION = 1.0f;     //If Lava collides with Stampo, this is the increment
    public float CROGIUOLO_SCORE_MAX = 100.0f;          //If Lava collides with Stampo, this is the increment
    public float CROGIUOLO_TRASL_MAX = 0.00f;       //If Lava collides with Stampo, this is the translation increment of the Ghisa level
    public float CROGIUOLO_TRASL_MIN = -0.415f;     //If Lava collides with Stampo, this is the translation increment of the Ghisa level
    public float CROGIUOLO_SPAWN_TIME = 10.0f;         //Each X seconds the Crogiuolo spawn a Stampo
}


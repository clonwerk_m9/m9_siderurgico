﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressaDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//void OnCollisionEnter(Collision obj)
	void OnTriggerEnter(Collider obj)
	{
		//Debug.Log ("OnCollisionEnter: "+ obj.gameObject.name);
		if (obj.gameObject.tag == "automatic")
			return;
        LingPressaCtrl ctrl = obj.gameObject.GetComponent<LingPressaCtrl>();
        if (ctrl == null) return;
        float score = ctrl.score;
        Destroy (obj.gameObject);
        //This event will be catched by the next game, that will initialize the new lingotto with this score value
        EventAggregator.lingDestroyed.Invoke(Utils.LEVA_PRESSA, score);
    }
}

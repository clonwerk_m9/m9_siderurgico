using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoundCtrl: MonoBehaviour{

    public AudioClip bgSound;

    public AudioClip JoinTeam;
    public AudioClip CrogiuoloVersamento;
    public AudioClip CrogiuoloPunti;
    public AudioClip RightTimeFeedback;
    public AudioClip WrongTimeFeedback;
    public AudioClip TrenoMovimenti;
    public AudioClip TrenoLingottoAtterra;
    public AudioClip TrenoMorsaLasciaLingotto;
    public AudioClip PressaAzione;
    public AudioClip PressaVapore;
    public AudioClip CesoiaIngranaggio;
    public AudioClip CesoiaCadutaFerro;

    public float timerVaporeDelay = 1;
    float timerVapore;
    public float timerGrogiuoloDropDelay = 1;
    float timerCrogiuolo;
    public bool playCrogiuoloDrop = false;
    public float timerPressaAzioneDelay;
    float timerPressaAzione;
    public bool playPressaAzione = false;

    //All SFX on fxSource
    //Single BG Sound on bgASource (it is in loop)
    public AudioSource fxSource;
    public AudioSource crogiuoloLoopDropSource;
    public AudioSource crogiuoloLoopPointsSource;
    public AudioSource bgSource;
    public AudioSource FL_crog, FR_treno, RL_presse, RR_cesoia;


    public static SoundCtrl Instance { get; private set; }
	public void Awake() { Instance = this; }

	public void OnEnable(){
        EventAggregator.leverPressed.AddListener(OnLeverPressed);
        EventAggregator.beginCrogiuoloPoint.AddListener(OnBeginCrogiuoloPoint);
        EventAggregator.endCrogiuoloPoint.AddListener(OnEndCrogiuoloPoint);
        EventAggregator.trenoStart.AddListener(OnTrenoStart);
        EventAggregator.trenoLingottoHitFloor.AddListener(OnTrenoLingottoHitFloor);
        EventAggregator.trenoPinzeDrop.AddListener(OnTrenoDrop);
        EventAggregator.autoPress.AddListener(OnAutoPress);
        EventAggregator.cesoiaIronHitFloor.AddListener(OnCesoiaIronHitFloor);
        EventAggregator.leverPressedAtRightTime.AddListener(OnLeverPressedAtRightTime);
        EventAggregator.leverPressedWrongTime.AddListener(OnLeverPressedWrongTime);
    }
    public void OnDisable(){
        EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
        EventAggregator.beginCrogiuoloPoint.RemoveListener(OnBeginCrogiuoloPoint);
        EventAggregator.endCrogiuoloPoint.RemoveListener(OnEndCrogiuoloPoint);
        EventAggregator.trenoStart.RemoveListener(OnTrenoStart);
        EventAggregator.trenoLingottoHitFloor.RemoveListener(OnTrenoLingottoHitFloor);
        EventAggregator.trenoPinzeDrop.RemoveListener(OnTrenoDrop);
        EventAggregator.autoPress.RemoveListener(OnAutoPress);
        EventAggregator.cesoiaIronHitFloor.RemoveListener(OnCesoiaIronHitFloor);
        EventAggregator.leverPressedAtRightTime.RemoveListener(OnLeverPressedAtRightTime);
        EventAggregator.leverPressedWrongTime.RemoveListener(OnLeverPressedWrongTime);
    }

    public void OnLeverPressedAtRightTime(int id)
    {
        if (RightTimeFeedback != null) {
            if (id == Utils.LEVA_CROGIUOLO) {
                FL_crog.PlayOneShot(RightTimeFeedback);
            }else if(id == Utils.LEVA_TRENO)
            {
                FR_treno.PlayOneShot(RightTimeFeedback);
            }else if (id == Utils.LEVA_PRESSA)
            {
                RL_presse.PlayOneShot(RightTimeFeedback);
            }else if (id == Utils.LEVA_CESOIA)
            {
                RR_cesoia.PlayOneShot(RightTimeFeedback);
            }

        }

    }
    public void OnLeverPressedWrongTime(int id)
    {
        if (WrongTimeFeedback != null) {
            if (id == Utils.LEVA_CROGIUOLO)
            {
                FL_crog.PlayOneShot(WrongTimeFeedback);
            }
            else if (id == Utils.LEVA_TRENO)
            {
                FR_treno.PlayOneShot(WrongTimeFeedback);
            }
            else if (id == Utils.LEVA_PRESSA)
            {
                RL_presse.PlayOneShot(WrongTimeFeedback);
            }
            else if (id == Utils.LEVA_CESOIA)
            {
                RR_cesoia.PlayOneShot(WrongTimeFeedback);
            }
        }
           
    }
    public void OnTrenoStart()
    {
        FR_treno.PlayOneShot(TrenoMovimenti);
    }
    public void OnTrenoLingottoHitFloor()
    {
        FR_treno.PlayOneShot(TrenoLingottoAtterra);
    }
    public void OnTrenoDrop()
    {
        FR_treno.PlayOneShot(TrenoMorsaLasciaLingotto);
    }
    public void OnAutoPress()
    {
        playPressaAzione = true;
        timerPressaAzione = timerPressaAzioneDelay;
    }
    public void OnCesoiaIronHitFloor()
    {
        RR_cesoia.PlayOneShot(CesoiaCadutaFerro);
    }

    public void Start()
    {
        if(CrogiuoloVersamento != null)
        crogiuoloLoopDropSource.clip = CrogiuoloVersamento;
        if (CrogiuoloVersamento != null)
            crogiuoloLoopPointsSource.clip = CrogiuoloPunti;
        if (CrogiuoloVersamento != null)
        {
            bgSource.clip = bgSound;
            bgSource.Play();
        }
    }

    public void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (PressaVapore == null && isHumanAction)
        {
            //We are in the idle state: This sound means that a player has just joined the team or
            //  he is the first player to join the team, no matter what is id value
            if (IntroMng.Instance.currState != IntroStates.AUTOPLAYGAME && IntroMng.Instance.currState != IntroStates.INTRO_VIDEO_1)
                return;
            if (id == Utils.LEVA_CROGIUOLO && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN) == 0)
                return;
            if (id == Utils.LEVA_TRENO && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN) == 0)
                return;
            if (id == Utils.LEVA_PRESSA && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN) == 0)
                return;
            if (id == Utils.LEVA_CESOIA && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 0)
                return;
            //We are in the idle state: This sound means that a player has just joined the team or
            //  he is the first player to join the team, no matter what is id value
            fxSource.PlayOneShot(JoinTeam);
            return;
        }
        if (id == Utils.LEVA_CROGIUOLO && playCrogiuoloDrop == false)
        {
            if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN) == 1)
                return;
            playCrogiuoloDrop = true;
            timerCrogiuolo = Utils2.Instance.so.CROGIUOLO_DRIPPING_TIME;
            crogiuoloLoopDropSource.Play();
        }
        if (id == Utils.LEVA_PRESSA)
        {
            if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN) == 1)
                return;
            playPressaAzione = true;
            timerPressaAzione = timerPressaAzioneDelay;
            timerVapore = 0.5f;
        }
        if (id == Utils.LEVA_CESOIA)
        {
            if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 1)
                return;
            RR_cesoia.PlayOneShot(CesoiaIngranaggio);
        }
    }

    public void OnBeginCrogiuoloPoint()
    {
        crogiuoloLoopPointsSource.Play();
    }
    public void OnEndCrogiuoloPoint()
    {
        crogiuoloLoopPointsSource.Stop();
    }

    private void Update()
    {
        if (timerCrogiuolo > 0)
        {
            timerCrogiuolo -= Time.deltaTime;
            if (timerCrogiuolo <= 0)
            {
                playCrogiuoloDrop = false;
                //crogiuoloLoopDropSource.Stop();
                crogiuoloLoopDropSource.PlayOneShot(CrogiuoloVersamento);
                crogiuoloLoopDropSource.PlayOneShot(CrogiuoloVersamento);
                crogiuoloLoopDropSource.PlayOneShot(CrogiuoloVersamento);
            }
        }
        if (timerVapore > 0)
        {
            timerVapore -= Time.deltaTime;
            if (timerVapore <= 0)
                RL_presse.PlayOneShot(PressaVapore);
        }
        if (playPressaAzione && timerPressaAzione > 0)
        {
            timerPressaAzione -= Time.deltaTime;
            if (timerPressaAzione <= 0)
            {
                RL_presse.PlayOneShot(PressaAzione);
                playPressaAzione = false;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMng : MonoBehaviour
{
    // There will be only one Instance of this class
    public static InputMng Instance { get; private set; }
    public void Awake() { Instance = this; }

    public bool Leva01_Crogiuolo;
    public bool Leva02_Treno;
    public bool Leva03_Pressa;
    public bool Leva04_Cesoia;
    public float ValidTresholdTime = 0.4f;      //If a lever is pressed with an interval less than this threshold, discard it
    float L0Timer, L1Timer, L2Timer, L3Timer;   

    public void OnEnable()
    {
        //EventAggregator.hoverButtonTriggered.AddListener(OnHoverButtonTriggered);
    }

    public void OnDisable()
    {
        //EventAggregator.hoverButtonTriggered.RemoveListener(OnHoverButtonTriggered);
    }

    void Start()
    {
        L0Timer = L1Timer = L2Timer = L3Timer = Time.time;
    }

    void Update()
    {
        if (Leva01_Crogiuolo)
        {
            if (Time.time - L0Timer > ValidTresholdTime)
            {
                L0Timer = Time.time;
                Leva01_Crogiuolo = false;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_CROGIUOLO, true);
            }
            else
                Leva01_Crogiuolo = false;
        }
        else if (Leva02_Treno)
        {
            if (Time.time - L1Timer > ValidTresholdTime)
            {
                L1Timer = Time.time;
                Leva02_Treno = false;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_TRENO, true);
            }
            else Leva02_Treno = false;
        }
        else if (Leva03_Pressa)
        {
            if (Time.time - L2Timer > ValidTresholdTime)
            {
                L2Timer = Time.time;
                Leva03_Pressa = false;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_PRESSA, true);
            }
            else Leva03_Pressa = false;
        }
        else if (Leva04_Cesoia)
        {
            if (Time.time - L3Timer > ValidTresholdTime)
            {
                L3Timer = Time.time;
                Leva04_Cesoia = false;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_CESOIA, true);
            }
            else Leva04_Cesoia = false;
        }
    }

    /*
	public void OnLevaCrogiuolo()
	{
		EventAggregator.leverPressed.Invoke(Utils.LEVA_CROGIUOLO);
	}

	public void OnLevaTreno()
	{
		EventAggregator.leverPressed.Invoke(Utils.LEVA_TRENO);
	}

	public void OnLevaPresse()
	{
		EventAggregator.leverPressed.Invoke(Utils.LEVA_PRESSA);
	}

	public void OnLevaCesoia()
	{
		EventAggregator.leverPressed.Invoke(Utils.LEVA_CESOIA);
	}
    */

    //AUTOMATE




}

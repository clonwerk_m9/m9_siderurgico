﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script invokes setActiveAutomatic event, which is handled by SetAutomatic.cs in AUTOMATIC_CR_root, AUTOMATIC_TR_root, AUTOMATIC_PR_root, AUTOMATIC_CE_root
public class AutomaticMng : MonoBehaviour {
    public KeyCode switchAutoCrogiuolo;
    public KeyCode switchAutoTreno;
    public KeyCode switchAutoPressa;
    public KeyCode switchAutoCesoia;
    public bool enableDebugAutoSwitch = false; //If true, we can switch auto/manual setting during runtime via keyboerd
    int autoCrogiuolo, autoTreno, autoPressa, autoCesoia; //1: auto, 0: manual

    void Start () {
        autoCrogiuolo = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN);
        autoTreno = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN);
        autoPressa = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN);
        autoCesoia = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN);

        Debug.Log(string.Format("AutomaticMng. Auto run on: {0} {1} {2} {3}", autoCrogiuolo, autoTreno, autoPressa, autoCesoia));

        EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_CROGIUOLO, autoCrogiuolo == 1);
        EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_TRENO, autoTreno == 1);
        EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_PRESSA, autoPressa == 1);
        EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_CESOIA, autoCesoia == 1);
    }
	
	void Update () {
        if (!enableDebugAutoSwitch) return;
        if (Input.GetKeyDown(switchAutoCrogiuolo))
        {
            autoCrogiuolo = 1 - autoCrogiuolo;
            EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_CROGIUOLO, autoCrogiuolo == 1);
        }
        else if (Input.GetKeyDown(switchAutoTreno))
        {
            autoTreno = 1 - autoTreno;
            EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_TRENO, autoTreno == 1);
        }
        else if (Input.GetKeyDown(switchAutoPressa))
        {
            autoPressa = 1 - autoPressa;
            EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_PRESSA, autoPressa == 1);
        }
        else if (Input.GetKeyDown(switchAutoCesoia))
        {
            autoCesoia = 1 - autoCesoia;
            EventAggregator.setActiveAutomatic.Invoke(Utils.LEVA_CESOIA, autoCesoia == 1);
        }

    }
}

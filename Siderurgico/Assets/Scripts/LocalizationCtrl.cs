﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/* Helping class to retrive correct localization text
*/
public class LocalizationCtrl : MonoBehaviour {

	// There will be only one Instance of this class
	public static LocalizationCtrl Instance { get; private set; }
	public void Awake() { Instance = this; }

	public Dictionary<string, string> texts = new Dictionary<string, string>();

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}

	public string getLocText(string HANDLE){
		if (texts.ContainsKey (HANDLE))
			return texts [HANDLE];
		else
			return HANDLE;
	}
}

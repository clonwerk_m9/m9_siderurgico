﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public enum _automateType { CROGIUOLO, TRENO, PRESSA, CESOIA };
public class SetAutomatic : MonoBehaviour {
    public _automateType automateType;

    public void OnEnable()
    {
        EventAggregator.setActiveAutomatic.AddListener(OnSetActiveAutomatic);
    }

    public void OnDisable()
    {
        EventAggregator.setActiveAutomatic.RemoveListener(OnSetActiveAutomatic);
    }

    void OnSetActiveAutomatic(int id, bool isActive)
    {
        if (id == Utils.LEVA_CROGIUOLO && automateType == _automateType.CROGIUOLO)
        {
            transform.GetChild(0).gameObject.SetActive(isActive);
        }
        else if (id == Utils.LEVA_TRENO && automateType == _automateType.TRENO)
        {
            transform.GetChild(0).gameObject.SetActive(isActive);
        }
        else if (id == Utils.LEVA_PRESSA && automateType == _automateType.PRESSA)
        {
            transform.GetChild(0).gameObject.SetActive(isActive);
        }
        else if (id == Utils.LEVA_CESOIA && automateType == _automateType.CESOIA)
        {
            transform.GetChild(0).gameObject.SetActive(isActive);
        }
    }
}

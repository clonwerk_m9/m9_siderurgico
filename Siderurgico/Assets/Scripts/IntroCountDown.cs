﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroCountDown : MonoBehaviour {
    Text t;

    void Start () {
        t = GetComponent<Text>();
	}
	
	void Update () {
		int secs = (int)IntroMng.Instance.videoCountdown;
        t.text = secs.ToString();
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 TRENO CHALLENGE
    1. Every time a Treno Ling hits the floor after being transported, the Right Range bar switch randomly between binary 1 and 2.
    2. Binary 3 is not used becaused is used for the automatic release of the Ling if the user fails to drop the Ling in the green range.
    2. Treno lever will release the Ling only if the lever is pulled between the start and the end of the bar (from red to red)
 */
public class RightRangeChanger : MonoBehaviour {
    public float Binary_1_Y, Binary_2_Y, Binary_3_Y;
    public float BinaryRange_1_Z, BinaryRange_2_Z, BinaryRange_3_Z;
	public Vector3 Binary_1_AutomaticCollider, Binary_2_AutomaticCollider, Binary_3_AutomaticCollider;
	public Transform automaticColliderRoot;
    public RectTransform rangeBar;
    public Transform rangeEmptyObjs;
    int currBinary;

    private void Start()
    {
        currBinary = 0;
        changeBinary();
    }

    public void OnEnable()
    {
        EventAggregator.trenoLingottoHitFloor.AddListener(OnTrenoLingottoHitFloor);
    }

    public void OnDisable()
    {
        EventAggregator.trenoLingottoHitFloor.RemoveListener(OnTrenoLingottoHitFloor);
    }

    void OnTrenoLingottoHitFloor()
    {
        changeBinary();
    }

    void changeBinary() {
        currBinary = (int)Random.Range(0.0f, 1.9f);
        //Debug.Log("currBinary" + currBinary);

        float newY = 0;
        float newZ_EmptyObjes = 0;
        if (currBinary == 0)
        {
            newY = Binary_1_Y;
            newZ_EmptyObjes = BinaryRange_1_Z;
			automaticColliderRoot.localPosition = Binary_1_AutomaticCollider;
        }
        else if (currBinary == 1)
        {
            newY = Binary_2_Y;
            newZ_EmptyObjes = BinaryRange_2_Z;
			automaticColliderRoot.localPosition = Binary_2_AutomaticCollider;
        }
        else if (currBinary == 2)
        {
            newY = Binary_3_Y;
            newZ_EmptyObjes = BinaryRange_3_Z;
            automaticColliderRoot.localPosition = Binary_3_AutomaticCollider;
        }

        rangeBar.anchoredPosition = new Vector2(newY, 0);
        rangeEmptyObjs.localPosition = new Vector3(0, -8.02f, newZ_EmptyObjes);
    }
}

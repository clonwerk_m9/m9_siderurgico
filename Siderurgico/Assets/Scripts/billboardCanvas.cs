﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class billboardCanvas : MonoBehaviour {
    Transform camT;
    public string camCrogiuoloName = "00 Camera";
    public string camTrenoName = "01 Camera";
    public string camPressaName = "02 Camera";
    public string camCesoiaName = "03 Camera";
    public Transform parentT;
    public float parentOffset;

    // Use this for initialization
    void Start () {
        if (this.gameObject.layer == LayerMask.NameToLayer("lingottoCrogiuolo"))
            camT = GameObject.Find(camCrogiuoloName).transform;
        else if (this.gameObject.layer == LayerMask.NameToLayer("lingottoPressa"))
            camT = GameObject.Find(camPressaName).transform;
        else if (this.gameObject.layer == LayerMask.NameToLayer("lingottoTreno"))
            camT = GameObject.Find(camTrenoName).transform;
        else if (this.gameObject.layer == LayerMask.NameToLayer("lingottoCesoia"))
            camT = GameObject.Find(camCesoiaName).transform;
    }

    // Update is called once per frame
    void Update () {
        if (!camT) return;

        transform.forward = camT.forward;
		transform.position = parentT.position + (camT.position - parentT.position).normalized*parentOffset + new Vector3(2.5f, 0f, 0f);
        /*
        new Vector3(parentT.position.x,
            parentT.position + camT.position - parentT.position,
            parentT.position.z);
        */
    }
}

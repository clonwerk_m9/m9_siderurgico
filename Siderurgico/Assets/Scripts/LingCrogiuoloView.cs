﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LingCrogiuoloView : LingView {
    public Transform ghisaLevelTransform;

    void Update ()
    {
        scoreText.enabled = ctrl.score > 0;
        scoreText.text = ctrl.score.ToString();

        ghisaLevelTransform.localPosition = new Vector3(0,
            Mathf.Lerp(Utils2.Instance.so.CROGIUOLO_TRASL_MIN, Utils2.Instance.so.CROGIUOLO_TRASL_MAX, ctrl.score/Utils2.Instance.so.CROGIUOLO_SCORE_MAX),
            0);
    }
}

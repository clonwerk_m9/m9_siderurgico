﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Creates a new Instance of Lingotto, everytime a previous Lingotto is destroyed
//For CROGIUOLO, The lingotto is spawned by StampoSpawn.cs, that spawn every Utils2.Instance.so.CROGIUOLO_SPAWN_TIME seconds
public class SpawnerLingCtrl : MonoBehaviour {
    public Transform TrenoSpawn;
    public Transform PressaSpawn;
    public Transform CesoiaSpawn;
    public Transform CesoiaSpawnParent;

    public GameObject TrenoLingGO;
    public GameObject PressaLingGO;
    public GameObject CesoiaLingGO;

    public GameObject TrenoWaitTextObj;
    public GameObject PressaWaitTextObj;
    public GameObject CesoiaWaitTextObj;

    private int countDestroyedCrog = 0; //ALESSIO

    public void OnEnable()
    {
        EventAggregator.lingDestroyed.AddListener(OnLingDestroyed);
    }

    public void OnDisable()
    {
        EventAggregator.lingDestroyed.RemoveListener(OnLingDestroyed);
    }

    void OnLingDestroyed(int id, float currScore)
    {
        if(id == Utils.LEVA_CROGIUOLO)
        {
            if (currScore < 0)
                return;
            if (currScore == 0)
                currScore = Utils2.Instance.so.CROGIUOLO_EMPTY_MINIMUMSCORE;
            if (countDestroyedCrog == 0)                   //ALESSIO
            {                                           //ALESSIO
                //NB: THIS IS PERFORMED ONLY ONCE: TrenoLingGO, the ingot of Treno,
                //  is usually instantiated every time Pinza returnback in its animation,
                //  in invokeLingotto. This is performed ONLY ONCE at the beginning,
                //  because PIna at the beginning is stopped
                //InvokeLingotto.onReturnBack() si occupa di instanziare lingotti per il treno successivi al primo
                GameObject newLing = Instantiate(TrenoLingGO, TrenoSpawn.position, TrenoSpawn.rotation);
                EventAggregator.lingCreated.Invoke(Utils.LEVA_TRENO);
                LingTrenoCtrl ctrl = newLing.GetComponent<LingTrenoCtrl>();
                ctrl.score = currScore;

                countDestroyedCrog++;

                //(ALESSIO)Disable waiting text. Scene starts with Waiting text enabled. Then it gets disabled at first ingot invoked of this session
                if (TrenoWaitTextObj != null) { TrenoWaitTextObj.active = false; }

            }//ALESSIO
            
        }
        else if (id == Utils.LEVA_TRENO)
        {
            GameObject newLing = Instantiate(PressaLingGO, PressaSpawn.position, PressaSpawn.rotation);
            LingPressaCtrl ctrl = newLing.GetComponent<LingPressaCtrl>();
			EventAggregator.lingCreated.Invoke(Utils.LEVA_PRESSA);

            ctrl.score = currScore;

            //(ALESSIO)Disable waiting text. Scene starts with Waiting text enabled. Then it gets disabled at first ingot invoked of this session
            if (PressaWaitTextObj != null) { PressaWaitTextObj.active = false; }
        }
        else if (id == Utils.LEVA_PRESSA)
        {
			//Debug.Log ("instantiate");
            GameObject newLing = Instantiate(CesoiaLingGO, CesoiaSpawn.position, CesoiaSpawn.rotation, CesoiaSpawnParent);
            LingCesoiaCtrl ctrl = newLing.GetComponent<LingCesoiaCtrl>();
			EventAggregator.lingCreated.Invoke(Utils.LEVA_CESOIA);

            ctrl.score = currScore;

            //(ALESSIO)Disable waiting text. Scene starts with Waiting text enabled. Then it gets disabled at first ingot invoked of this session
            if (CesoiaWaitTextObj != null) { CesoiaWaitTextObj.active = false; }
        }
    }

}

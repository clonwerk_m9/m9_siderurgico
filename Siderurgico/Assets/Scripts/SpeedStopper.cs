﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Take the Crogiuolo Lingotto that hits itself and slow it down to make the rate of destroyed Crogiuolo lingotti constant
public class SpeedStopper : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        LingCrogiuoloCtrl lingCtrl = other.gameObject.GetComponentInParent<LingCrogiuoloCtrl>();
        if (lingCtrl == null)
            return;
        lingCtrl.stopLing();
    }
}

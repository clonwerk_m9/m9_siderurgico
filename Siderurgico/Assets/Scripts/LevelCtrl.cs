﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCtrl : MonoBehaviour {

	// There will be only one Instance of this class
	public static LevelCtrl Instance { get; private set; }
	public void Awake() { Instance = this; }

    public bool TrenoStarted = false;
    public Animator TrenoAnim0;
	public KeyCode restartFromIntroKey;
    //Tot score is displayed as PUNTI/SCORE in the UI
	public int totScore;
    //There are partial scores: how good is this player? How many times pressed the lever at right time?
	public float CrogiuoloScore, TrenoScore, PressaScore, CesoiaScore;
    public bool CrogiuoloEnded, TrenoEnded, PressaEnded, CesoiaEnded; //These variables are checked by EndScoreCtrl
    public bool gameEnded = false;
    public bool[] isAutoDisplay; //isAutoDisplay[0] == true if Crogiuolo is an Automatic station

    //isActive == true if the display is human
    void OnSetActiveAutomatic(int id, bool isActive)
    {
        if(isAutoDisplay.Length < 4)
       isAutoDisplay = new bool[4];
        if (id == Utils.LEVA_CROGIUOLO && isActive)
            isAutoDisplay[0] = true;
        else if (id == Utils.LEVA_TRENO && isActive)
            isAutoDisplay[1] = true;
        else if (id == Utils.LEVA_PRESSA && isActive)
            isAutoDisplay[2] = true;
        else if (id == Utils.LEVA_CESOIA && isActive)
            isAutoDisplay[3] = true;
    }

    private void Start()
    {
        CrogiuoloEnded = TrenoEnded = PressaEnded = CesoiaEnded = false;
    }
    void OnAddPartialPoint(float currScore)
	{
		totScore += (int)currScore;
	}

    public void OnEnable()
    {
		EventAggregator.lingDestroyed.AddListener(OnLingDestroyed);
		EventAggregator.gameRestart.AddListener(OnGameRestart);
		EventAggregator.endGame.AddListener(OnEndGame);
		EventAggregator.addPartialPoint.AddListener(OnAddPartialPoint);
        EventAggregator.leverPressedAtRightTime.AddListener(OnLeverPressedAtRightTime);
        EventAggregator.partialGameEnded.AddListener(OnPartialGameEnded);
        EventAggregator.setActiveAutomatic.AddListener(OnSetActiveAutomatic);
    }

    public void OnDisable()
    {
		EventAggregator.lingDestroyed.RemoveListener(OnLingDestroyed);
		EventAggregator.gameRestart.RemoveListener(OnGameRestart);
		EventAggregator.endGame.RemoveListener(OnEndGame);
		EventAggregator.addPartialPoint.RemoveListener(OnAddPartialPoint);
        EventAggregator.leverPressedAtRightTime.RemoveListener(OnLeverPressedAtRightTime);
        EventAggregator.partialGameEnded.RemoveListener(OnPartialGameEnded);
        EventAggregator.setActiveAutomatic.RemoveListener(OnSetActiveAutomatic);
    }

    void OnPartialGameEnded(int id)
    {
        if (id == Utils.LEVA_TRENO)
        {
            TrenoAnim0.SetTrigger("EndTreno");
            TrenoEnded = true;
            EventAggregator.updateFinalScorePanels.Invoke();
        }
        else if (id == Utils.LEVA_PRESSA)
        {
            PressaEnded = true;
            EventAggregator.updateFinalScorePanels.Invoke();
        }
        else if (id == Utils.LEVA_CESOIA)
        {
            CesoiaEnded = true;
            EventAggregator.updateFinalScorePanels.Invoke();
        }
        else if (id == Utils.LEVA_CROGIUOLO)
        {
            CrogiuoloEnded = true;
            EventAggregator.updateFinalScorePanels.Invoke();
        }
    }

    void OnLeverPressedAtRightTime(int id)
    {
        //For Treno, Pressa and Cesoia, we'll know if the player used the lever at right time if this event is fired
        if (id == Utils.LEVA_TRENO && !TrenoEnded)
        {
            TrenoScore++;
        }
        else if (id == Utils.LEVA_PRESSA && !PressaEnded)
        {
            PressaScore++;
        }
        else if (id == Utils.LEVA_CESOIA && !CesoiaEnded)
        {
            CesoiaScore++;
        }
        else if (id == Utils.LEVA_CROGIUOLO && !CrogiuoloEnded)
        {
            CrogiuoloScore++;
            //Debug.Log("CrogiuoloScore: " + CrogiuoloScore);
        }
        //CrogiuoloScore is calculated from how much Total Ferro fuso filled All the Stamps
    }

    void OnEndGame()
	{
		gameEnded = true;
	}

    void OnLingDestroyed(int id, float currScore)
    {
        if (id == Utils.LEVA_CROGIUOLO)
        {
            if (!TrenoStarted)
            {
                TrenoStarted = true;
                TrenoAnim0.SetTrigger("StartTreno");
            }
        }
        else if (id == Utils.LEVA_TRENO)
        {
        }
        else if (id == Utils.LEVA_PRESSA)
        {
        }
    }

	void OnGameRestart()
	{
		SceneManager.LoadScene (1);
	}

	void Update()
	{
		if (Input.GetKeyDown (restartFromIntroKey))
			EventAggregator.gameRestart.Invoke ();
	}


}

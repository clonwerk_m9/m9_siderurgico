﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//NB: LingCesoiaCtrl is on Biletta, is not on a persistent scene gameobject!
public class CesoiaCtrl : MonoBehaviour {
    // There will be only one Instance of this class
    public static CesoiaCtrl Instance { get; private set; }
    public void Awake() { Instance = this; }

    public float endTimer; //After the last biletta start a counter initialized with this value
    public bool processEndTimer = false;
    public int bilettePassed = 0;
    bool ended = false;

    void Start () {
        endTimer = Utils2.Instance.so.END_DISPLAYSCORE_TIMER;

    }

    void Update () {
        if (bilettePassed == Utils2.Instance.so.LINGOTTI_COUNTER)
            processEndTimer = true;

        if (processEndTimer && !ended)
        {
            if (endTimer < 0)
            {
                ended = true;
                EventAggregator.partialGameEnded.Invoke(Utils.LEVA_CESOIA);
                EventAggregator.endGame.Invoke();
            }

            endTimer -= Time.deltaTime;
        }
    }
}

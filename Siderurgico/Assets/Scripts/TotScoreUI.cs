﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotScoreUI : MonoBehaviour {
    Text currScoreText;
    int totScore;

    public void OnEnable()
    {
        EventAggregator.addPartialPoint.AddListener(OnAddPartialPoint);
    }

    public void OnDisable()
    {
        EventAggregator.addPartialPoint.RemoveListener(OnAddPartialPoint);
    }

    void OnAddPartialPoint(float currScore)
    {
        totScore += (int)currScore;
        currScoreText.text = totScore.ToString();
    }

    // Use this for initialization
    void Start () {
        currScoreText = GetComponent<Text>();
        totScore = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAutoFeedbackAtEndScore : MonoBehaviour {
    public GameObject partialScoreDisplay;
    [Range(1, 4)]
    public int displayID; //displayID == 0 if this DisplayAutoFeedbackAtEndScore is attached to display 0 (crogiuolo)

    void Update () {
        bool isAutomatic = LevelCtrl.Instance.isAutoDisplay[displayID - 1];
        transform.GetChild(0).gameObject.SetActive(isAutomatic);
        transform.GetComponent<Image>().enabled = !isAutomatic;
        partialScoreDisplay.SetActive(!isAutomatic);
        
    }
}

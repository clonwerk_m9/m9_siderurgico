﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disableWaitTextWhenAuto : MonoBehaviour {

    public _automateType automateType;
    int autoTreno, autoPressa, autoCesoia; //1: auto, 0: manual

    void Start()
    {
        autoTreno = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN);
        autoPressa = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN);
        autoCesoia = PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN);

        if ((automateType == _automateType.TRENO && autoTreno == 1) ||
            (automateType == _automateType.PRESSA && autoPressa == 1) ||
            (automateType == _automateType.CESOIA && autoCesoia == 1))
            gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadIntro : MonoBehaviour {
    float startTime;

    void Start () {
        startTime = Time.time;
	}
	
	void Update () {
        if(Time.time - startTime > 2)
            SceneManager.LoadScene(1);
    }
}

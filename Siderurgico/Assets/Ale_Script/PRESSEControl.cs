﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PRESSEControl : MonoBehaviour {

	public bool isAutomatic;

	[Range(0.0f, 1.0f)]	public float sliderPressa, sliderPosition;


	public Transform pressaPos;
	public float posStart, posEnd;

	public Transform[] modulo;
	public float moduloEnd;

	public Transform[] piastra;
	public float piastraEnd;

	public Transform[] rotatorA;
	public float rotA_Start, rotA_End;

	public Transform[] rotatorB;
	public float rotB_Start, rotB_End;


	private ParticleSystem[] systems;
	private bool explode = false;
	private bool active = false;

    public Animator anim;

    public void OnEnable()
    {
        EventAggregator.leverPressed.AddListener(OnLeverPressed);
    }

    public void OnDisable()
    {
        EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
    }

    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER3_HUMAN) == 1)
            return;
        if (isAutomatic == true)
			return;
		
        if (id == Utils.LEVA_PRESSA)
        {
            //Only PressaPlayer animator has PressPlayer trigger. The others have Press trigger
            anim.SetTrigger("PressPlayer");
        }
    }

    // Use this for initialization
    void Start () {
		sliderPressa = 0;

		systems = GetComponentsInChildren<ParticleSystem>();
		/*foreach (ParticleSystem system in systems)
		{
			ParticleSystem.MainModule mainModule = system.main;
			system.Clear();
			system.Play();
			//mainModule.startSizeMultiplier *= multiplier;
			//mainModule.startSpeedMultiplier *= multiplier;
			//mainModule.startLifetimeMultiplier *= Mathf.Lerp(multiplier, 1, 0.5f);
		}*/
	}
	
	// Update is called once per frame
	void Update () {

		float sLer = Mathf.Lerp(0.0f, moduloEnd, sliderPressa);
		float pLer = Mathf.Lerp(0.0f, piastraEnd, (sliderPressa*2)-1);
		float rALer = Mathf.Lerp(rotA_Start, rotA_End, sliderPressa);
		float rBLer = Mathf.Lerp(rotB_Start, rotB_End, sliderPressa);

		if (sliderPressa >= 1 && active != true) 
		{ 
			explode = active = true; 
		}

		if (sliderPressa <= 0)
		{
			active = false;
		}

		if (explode == true) 
		{
			foreach (ParticleSystem system in systems)
			{
				ParticleSystem.MainModule mainModule = system.main;
				//system.Clear();
				system.Play();
				explode = false;
			}
		}



		for (int i=0; i<2; i++)
		{
			modulo[i].localPosition = new Vector3(0.0f , 0.0f, sLer);

			piastra[i].localPosition = new Vector3(0.0f , 0.0f, pLer);
			rotatorA[i].localRotation = Quaternion.Euler(rALer, 0.0f, 0.0f);
			rotatorB[i].localRotation = Quaternion.Euler(rBLer, 0.0f, 0.0f);
		}

		float posLer = Mathf.Lerp(posStart, posEnd, sliderPosition);
		pressaPos.localPosition = new Vector3(0.0f , 0.0f, posLer);



			
	}
	void OnTriggerEnter(Collider other)
	{
		if (isAutomatic == false)
			return;
		if (other.gameObject.tag == "automatic") 
			return;
		


		Animator a = GetComponent<Animator>();
		a.SetTrigger("Press");
        EventAggregator.autoPress.Invoke();
	}


}

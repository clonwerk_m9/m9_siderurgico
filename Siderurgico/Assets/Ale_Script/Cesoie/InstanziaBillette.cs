﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanziaBillette : MonoBehaviour {


	public GameObject billetta;
	public Transform originBillettaA, originBillettaB;
	public bool trig;
	public float power;
    
    public ParticleSystem steam;

	public float boundMin, boundMax;
	private Animator cutAni;
	private bool cut;

    public void OnEnable()
    {
		EventAggregator.leverPressed.AddListener(OnLeverPressed);
		EventAggregator.missedCesoia.AddListener(OnMissedCesoia);

    }

    public void OnDisable()
    {
		EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
		EventAggregator.missedCesoia.RemoveListener(OnMissedCesoia);
    }

    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER4_HUMAN) == 1)
            return;
        if (id != Utils.LEVA_CESOIA)
            return;
        OnTrig();
    }

	// Use this for initialization
    void Start () {
		cutAni = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
        /*
		if (trig == true) {
			OnTrig ();
			trig = false;
		}
        */
	}

	void OnTrig()
	{
		cutAni.SetTrigger ("trigCesoia");
		steam.Play ();

	}


	void OnMissedCesoia(GameObject go)
	{
		float billettaScale;
		float maxScale = 1.2f;
		billettaScale = 0;
        //[3/25.04.29]
        //Destroy (go);
        GameObject a, b;
		a = Instantiate (billetta, originBillettaA.position, originBillettaA.rotation);
		a.transform.localScale = new Vector3 (1f, 1f, maxScale);
		Rigidbody force = a.GetComponent<Rigidbody> ();
		force.AddForce (power, 0f, 0f);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag =="automatic") 
			return;
		if (other.gameObject.tag == "billettaL") 
		{
			float billettaScale;
			float maxScale = 1.2f;
			float posX = other.transform.position.x;
			//float billettaScale = Mathf.Clamp(((posX - boundMin) / (boundMax - boundMin))*2f , 0f , 2f);
			float distance = (posX - originBillettaA.position.x);
			float maxDistance = 6.0f;
			distance += maxDistance;
			distance /= maxDistance * 2f;
			//distance /= 5.0f;
			billettaScale = Mathf.Lerp (0.1f, maxScale, Mathf.Abs (distance));
            /*
        if (distance > 0)
            billettaScale = Mathf.Lerp(0.6f, 1.33f, Mathf.Abs(distance));
        else
            billettaScale = Mathf.Lerp(0.6f, 1.33f, 1.0f-Mathf.Abs(distance));
            */
            //TODO: Inserire calcoli bontà taglio
            //Avremo un range verde [1-range/2, 1+range/2], tutto quello che non è nel range è rosso e dimezza il punteggio

            //Debug.Log (other.transform.position.x);
            //Debug.Log (billettaScale);


            //[4/25.04.29]
            LingCesoiaCtrl lingCesoiaCtrl = other.gameObject.GetComponent<LingCesoiaCtrl>();
            if (lingCesoiaCtrl != null && lingCesoiaCtrl.pressed == false)
                EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_CESOIA);

            Destroy (other.gameObject);
            GameObject a, b;
			a = Instantiate (billetta, originBillettaA.position, originBillettaA.rotation);
			a.transform.localScale = new Vector3 (1f, 1f, billettaScale);
			Rigidbody force = a.GetComponent<Rigidbody> ();
			force.AddForce (power, 0f, 0f);
            Vector3 newPos = new Vector3(originBillettaA.position.x-(billettaScale), originBillettaA.position.y, originBillettaA.position.z);
			b = Instantiate (billetta, newPos, originBillettaB.rotation);
			b.transform.localScale = new Vector3 (1f, 1f, maxScale - billettaScale);
		}
	}

}

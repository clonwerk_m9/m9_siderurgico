﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PozzoApriChiudi : MonoBehaviour {

	private Animator pozzoOpener;
	public float slider;
	// Use this for initialization
	void Start () {
		pozzoOpener = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		pozzoOpener.Play ("AniPozzo", -1, slider);
	}
}

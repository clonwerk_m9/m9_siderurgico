﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StampoSpawner : MonoBehaviour {

	public GameObject stampo;
	int counter = 0;
	//private GameObject _stampi;
	float timer;
	bool spawnIsActive = true;

	// Use this for initialization
	void Start () {
        timer = 0;
		spawnIsActive = true;
	}


	// Update is called once per frame
	void Update () {
		if (!spawnIsActive)
			return;
		
        timer -= Time.deltaTime;
		if (timer <= 0)
		{
			counter++;
			//If we reach the maximum num of Stampi, stop spawn stampi
			//LingCesoiaCtrl will check bilettePassed number to invoke the end of the game
			if (counter == Utils2.Instance.so.LINGOTTI_COUNTER)
				spawnIsActive = false;
			
            timer = Utils2.Instance.so.CROGIUOLO_SPAWN_TIME;
			GameObject obj = GameObject.Instantiate(stampo);
			EventAggregator.lingCreated.Invoke(Utils.LEVA_CROGIUOLO);
			obj.transform.position = this.transform.position;
			obj.transform.transform.parent = this.transform;
		}
	}
}

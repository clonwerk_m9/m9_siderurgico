﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhisaParticle : MonoBehaviour {

	public ParticleSystem ghisa;
	public bool tog = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(ghisa != null)
		{
			if (transform.eulerAngles.x > 45.0f)
				ghisa.Play();
		}
	}
}

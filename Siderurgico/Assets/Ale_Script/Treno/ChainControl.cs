﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainControl : MonoBehaviour {

	public Material chainMat;
	public float xOffset, chainSpeed, rotSpeed;
	public GameObject[] cogs;
	private float yOffset;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		yOffset = transform.localPosition.z*chainSpeed;

		foreach(GameObject c in cogs)
		{
			c.transform.localRotation = Quaternion.Euler(0f,90f,yOffset*rotSpeed);
		}
		Vector2 texOffset = new Vector2 (xOffset, yOffset);
		chainMat.SetTextureOffset ("_MainTex", texOffset);
	}
}

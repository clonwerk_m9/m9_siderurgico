﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour {

	public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionStay(Collision obj)
	{
		obj.rigidbody.velocity = speed * transform.forward;
	}
	/*
	void OnCollisionExit(Collision obj)
	{
		obj.rigidbody.velocity = Vector3.zero;
	}*/
}


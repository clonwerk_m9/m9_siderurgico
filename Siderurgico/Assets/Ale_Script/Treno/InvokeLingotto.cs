﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeLingotto : MonoBehaviour {

	public GameObject lingotto;
	public GameObject lingottoFisso, lingottoScore;
	public Light lightLing;

    public bool trigLingotto;
	private Renderer lingRend;
    bool dropped = false;

    public Transform TrenoSpawn;
    public GameObject TrenoLingGO;
    int lingottiTaken = 0;
    public Animator anim;//Treno animator to stop if we reach Utils2.Instance.so.LINGOTTI_COUNTER == lingottiTaken

    public void OnEnable()
    {
        EventAggregator.leverPressed.AddListener(OnLeverPressed);
        EventAggregator.trenoPinzeDrop.AddListener(OnTrenoPinzeDrop);
        EventAggregator.trenoPinzeTake.AddListener(OnTrenoPinzeTake);
    }

    public void OnDisable()
    {
        EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
        EventAggregator.trenoPinzeDrop.RemoveListener(OnTrenoPinzeDrop);
        EventAggregator.trenoPinzeTake.RemoveListener(OnTrenoPinzeTake);
    }

    void OnTrenoPinzeDrop()
    {
        dropped = true;
    }
    void OnTrenoPinzeTake()
    {
        dropped = false;
    }
    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (id != Utils.LEVA_TRENO)
            return;
        if (isHumanAction && PlayerPrefs.HasKey(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN) && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER2_HUMAN) == 1)
            return;
        OnInvokeLingotto();
    }

    // Use this for initialization
    void Start () {
		lingRend = lingottoFisso.GetComponent<Renderer> ();
	}
	
    //Pinza has lingotto, is at the end of the journey and user didn't activate the lever
	void OnReturnBack()
	{
        if (dropped == false)
        {
            EventAggregator.trenoPinzeDrop.Invoke();
            EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_TRENO);
        }
        lingottiTaken++;
        if (TrenoSpawn != null && Utils2.Instance != null && Utils2.Instance.so != null && TrenoLingGO != null && lingottiTaken < Utils2.Instance.so.LINGOTTI_COUNTER)
        {
            GameObject newLing = Instantiate(TrenoLingGO, TrenoSpawn.position, TrenoSpawn.rotation);
            EventAggregator.lingCreated.Invoke(Utils.LEVA_TRENO);
        }
        else
        {
            //TODO (Search for [1/25.04.19] in doc 'M9 TODO Siderurgico')
            //[1/25.04.19]
            if (anim != null)
                anim.enabled = false;
        }
        //LingTrenoCtrl ctrl = newLing.GetComponent<LingTrenoCtrl>();
    }

    //Pinza is about to take the new lingotto
    void OnRestart()
	{
        EventAggregator.trenoPinzeTake.Invoke();
	}

    //Player activates lever
    public void OnInvokeLingotto()
	{
        EventAggregator.trenoPinzeDrop.Invoke();
    }

}

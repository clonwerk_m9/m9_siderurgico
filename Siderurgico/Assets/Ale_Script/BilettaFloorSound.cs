﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BilettaFloorSound : MonoBehaviour {

    private void OnTriggerEnter(Collider c)
    {
        if (c.tag == "scivolo")
            EventAggregator.cesoiaIronHitFloor.Invoke();
    }
}

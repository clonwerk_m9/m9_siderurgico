﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum _crogState { IDLE, MOVEFORWARD, DRIPPING, MOVEBACKWARD}

[ExecuteInEditMode]
public class Crog_Ruota : MonoBehaviour {
	public float timeSpeed;
	public Transform ghisa;
	public Transform rotCrog;
	public GameObject lEmit, lavaObj;

	private ParticleSystem liquid;
	private LensFlare lf;
	//private int count = 0;
	[Range(0.0f, 1.0f)]
	public float mainSlider;

	private Vector4 lavaPosA; 
	public Vector4 lavaPosB; 
	private Material lavaMat;

    _crogState crogState = _crogState.IDLE;
    float crogStateTimer = 0;

    public void OnEnable()
    {
        EventAggregator.leverPressed.AddListener(OnLeverPressed);
    }

    public void OnDisable()
    {
        EventAggregator.leverPressed.RemoveListener(OnLeverPressed);
    }

    void  OnLeverPressed(int id, bool isHumanAction)
    {
        if (isHumanAction && PlayerPrefs.GetInt(Utils2.Instance.so.PPKEY_PLAYER1_HUMAN) == 1)
            return;
        if (id != Utils.LEVA_CROGIUOLO) return;
        if (crogState != _crogState.IDLE) return;
        crogState = _crogState.MOVEFORWARD;
        crogStateTimer = Utils2.Instance.so.CROGIUOLO_MOVEFORWARD_TIME;
    }

    // Use this for initialization
    void Start () {
		liquid = lEmit.GetComponent<ParticleSystem>();
		lf = lEmit.GetComponent<LensFlare>();

		lavaPosA = new Vector4 (1f, 1f, -1.2f, -2f);
		lavaPosB = new Vector4 (1f, 2f, -1.2f, -3f);
		lavaMat = lavaObj.GetComponent<Renderer>().sharedMaterial;
	}
	
    void checkCrogiuoloState()
    {
        if (crogState != _crogState.IDLE)
        {
			crogStateTimer -= Time.deltaTime;
            if (crogState == _crogState.MOVEFORWARD)
            {
                mainSlider = (Utils2.Instance.so.CROGIUOLO_MOVEFORWARD_TIME - crogStateTimer) / Utils2.Instance.so.CROGIUOLO_DRIPPING_TIME;
                if (crogStateTimer <= 0)
                {
                    crogState = _crogState.DRIPPING;
                    crogStateTimer = Utils2.Instance.so.CROGIUOLO_DRIPPING_TIME;
                }
            }
            else if (crogState == _crogState.DRIPPING)
            {
                mainSlider = 1;
                if (crogStateTimer <= 0)
                {
                    crogState = _crogState.MOVEBACKWARD;
                    crogStateTimer = Utils2.Instance.so.CROGIUOLO_MOVEBACK_TIME;
                }
            }
            else if (crogState == _crogState.MOVEBACKWARD)
            {
                mainSlider = (crogStateTimer) / Utils2.Instance.so.CROGIUOLO_MOVEBACK_TIME;
                if (crogStateTimer <= 0)
                {
                    crogState = _crogState.IDLE;
                    crogStateTimer = 0;
                }
            }
        }
    }
	// Update is called once per frame
	void Update () {
        checkCrogiuoloState();

		//prende materiale lava
		//Material mat = lavaObj.GetComponent<Renderer>().sharedMaterial;
		//if (!mat) {	return;		}
		//Vector4 waveSpeed = mat.GetVector("WaveSpeed");

		transform.localRotation = Quaternion.Euler(.5f, .5f, .5f);
		Quaternion rotIn = Quaternion.Euler(0,0,0);
		Quaternion rotOut = Quaternion.Euler(30,0,0);
		Quaternion rotOutInv = Quaternion.Euler(-30,0,0);
		float ghisaScale = Mathf.Lerp(1.2f, 1.4f, mainSlider);

		
		transform.localPosition = Vector3.Lerp(new Vector3(.0f, .0f, .0f), new Vector3(.0f, .0f, .5f), mainSlider);
		rotCrog.localRotation = Quaternion.Lerp(rotIn, rotOut, mainSlider*timeSpeed);	//LOCAL ROTATION**********************************************************
		ghisa.localRotation = Quaternion.Lerp(rotIn, rotOutInv, mainSlider);

		Vector4 waveSpeed = Vector4.Lerp (lavaPosA, lavaPosB, mainSlider);
		lavaMat.SetVector ("WaveSpeed", waveSpeed);

		if (lf != null)
		{
			lf.brightness = Mathf.Lerp(-0.25f, .25f, mainSlider);
		}

		ghisa.localScale = new Vector3(1.2f, 1.2f, ghisaScale);

		if(mainSlider == 1)
		{
			liquid.Emit(50);
		
		}
		else
		{

			//liquid.Stop();
		}



	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class ParticleTrigger : MonoBehaviour {

	public ParticleSystem shower;
	List<ParticleCollisionEvent> collisionEvent;

    float stopCollisionTimer;

	void Start()
	{
		collisionEvent = new List<ParticleCollisionEvent>();
	}


	void OnParticleCollision(GameObject other)
	{
        if(stopCollisionTimer <= 0)
            EventAggregator.beginCrogiuoloPoint.Invoke();
        stopCollisionTimer = 0.5f;

        if (other.gameObject.CompareTag("ghisaLevel"))
		{
            //print(other.name);
            LingCrogiuoloCtrl ctrl = other.gameObject.GetComponentInParent<LingCrogiuoloCtrl>();
			if (ctrl != null && ctrl.hitted == false) {
                ctrl.hitted = true;
                //ctrl.score += Utils2.Instance.so.CROGIUOLO_SCORE_INC_ONCOLLISION;
                ctrl.score = Utils2.Instance.so.CROGIUOLO_SCORE_MAX;
                //LevelCtrl.Instance.CrogiuoloScore += Utils2.Instance.so.CROGIUOLO_SCORE_INC_ONCOLLISION;
                EventAggregator.leverPressedAtRightTime.Invoke(Utils.LEVA_CROGIUOLO);
			}
            //other.transform.Translate(.0f, .004f, .0f);
        }
        /*
		else
		{
			ParticlePhysicsExtensions.GetCollisionEvents(this.GetComponent<ParticleSystem>(), other, collisionEvent);
			for ( int i=0; i < collisionEvent.Count; i++)
			{
				
				EmitAtLocation(collisionEvent[i]);
			}
		
		}
        */
	}

	void EmitAtLocation(ParticleCollisionEvent pce)
	{
		shower.transform.position = pce.intersection;

		shower.Emit(2);
	}

    void Update()
    {
        if(stopCollisionTimer > 0)
        {
            stopCollisionTimer -= Time.deltaTime;
            if(stopCollisionTimer <= 0)
            {
                EventAggregator.endCrogiuoloPoint.Invoke();
            }
        }

    }

    /*
	public ParticleSystem ps;

	

	ParticleSystem ps;
	List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();
	List<ParticleSystem.Particle> exit = new List<ParticleSystem.Particle>();


	void OnEnable()
	{
		ps = GetComponent<ParticleSystem>();
	}

	void OnParticleTrigger()
	{
		int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
		print(numEnter);
		int numExit = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Exit, exit);
		print(numExit);
	}
	*/
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crog_Rulli : MonoBehaviour {


	public Material rulli;
	public float scrollSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float offset = Time.time * scrollSpeed;
		rulli.mainTextureOffset = new Vector2(offset, 0f);
	}
}

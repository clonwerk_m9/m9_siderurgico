﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void TriggerEventDelegate();

public class AutomateClient : MonoBehaviour
{
    _automateType automateType;
    public GameObject AutomaticFeedbackUI;

    private void Start()
    {
        automateType = transform.parent.GetComponent<SetAutomatic>().automateType;
    }

    void OnTriggerEnter(Collider other)
	{
		if(other.tag != "automatic")
			return;

        if (automateType == _automateType.CROGIUOLO)
            EventAggregator.leverPressed.Invoke(Utils.LEVA_CROGIUOLO, false);
        else if (automateType == _automateType.TRENO)
            EventAggregator.leverPressed.Invoke(Utils.LEVA_TRENO, false);
        else if (automateType == _automateType.PRESSA)
            EventAggregator.leverPressed.Invoke(Utils.LEVA_PRESSA, false);
        else if (automateType == _automateType.CESOIA)
            EventAggregator.leverPressed.Invoke(Utils.LEVA_CESOIA, false);
    }
}
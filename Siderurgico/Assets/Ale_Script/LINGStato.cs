﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LINGStato : MonoBehaviour {


	//[Range(0.0f, 1.0f)]	public float stateAB, stateBC, stateCD;
	//public Vector2 xBound, yBound;
	public Vector3[] scale = new Vector3[4];
	public float speed;
	public int state;
	private Vector3 stateScale;
    public Transform meshObj;
	private Animator slideLing;

	// Use this for initialization
	void Start () {
		state = 0;
		stateScale = scale [0];
		slideLing = GetComponent<Animator> ();

	}
	// Update is called once per frame
	void Update () {

		switch (state) {
		case 0:
			stateScale = scale [0];
            break;
		case 1:
			stateScale = Vector3.Lerp (stateScale, scale [1], Time.deltaTime * speed);
                //slideLing.SetTrigger ("slideUp");
                
			break;
		case 2:
			stateScale = Vector3.Lerp (stateScale, scale [2], Time.deltaTime * speed);
            
			break;
		case 3:
			stateScale = Vector3.Lerp (stateScale, scale [3], Time.deltaTime * speed);
			break;

	
		}
   
        meshObj.localScale = stateScale;
	}

	void OnTriggerEnter (Collider other)
	{
		//Debug.Log (collision.gameObject.name);
		switch (other.gameObject.name)
		{
		case "PRESSA_1":
			state = 1;
            break;
		case "PRESSA_Player":
			state = 2;

			break;
		case "PRESSA_2":
			state = 3;
			break;
		
		}
	}

    IEnumerator Example()
    {
        print(Time.time);
        yield return new WaitForSeconds(5);
        print(Time.time);
    }
}


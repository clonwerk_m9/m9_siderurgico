﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RULLIRotation : MonoBehaviour {

	private Transform[] rulli;
	public float speed;
	// Use this for initialization
	void Start () {
		rulli = GetComponentsInChildren<Transform>();
		foreach (Transform r in rulli)
		{
			if (r != this.transform)
			{
				r.localRotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
			}

		}
	}
	
	// Update is called once per frame
	void Update () {

		foreach (Transform r in rulli)
		{
			
			if (r != this.transform)
			{
				r.Rotate(0.0f, 0.0f, speed);
			}

		}
	}
}

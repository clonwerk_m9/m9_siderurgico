﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomateWrongClient : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
	{
        GameObject lingCrogiuolo = other.gameObject;
        LingCrogiuoloCtrl ctrl = lingCrogiuolo.GetComponentInParent<LingCrogiuoloCtrl>();
        if (ctrl == null) return;
        if (ctrl.score == Utils2.Instance.so.CROGIUOLO_EMPTY_MINIMUMSCORE && other.tag == "automatic")
        {
            //User missed right time
            EventAggregator.leverPressedWrongTime.Invoke(Utils.LEVA_CROGIUOLO);
        }
    }
}
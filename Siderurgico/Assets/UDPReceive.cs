﻿/*

        -----------------------
        UDP-Receive (send to)
        -----------------------
        // [url]http://msdn.microsoft.com/de-de/library/bb979228.aspx#ID0E3BAC[/url]


        // > receive
        // 127.0.0.1 : 8051

        // send
        // nc -u 127.0.0.1 8051

    */
using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine.UI;

public class UDPReceive : MonoBehaviour {

    public static UDPReceive Instance { get; private set; }
    public void Awake() { Instance = this; }

    public bool Sens1 = false;
    public bool Sens2=false;
	public bool Sens3=false;
	public bool Sens4=false;

    public bool pSens1 = false;
    public bool pSens2 = false;
    public bool pSens3 = false;
    public bool pSens4 = false;

    public bool INVERT_INPUT = false;
    public KeyCode debugKey;
    public KeyCode invertKey; //switch INVERT_INPUT
    public GameObject debugCanvas;
    public Image DImg1, DImg2, DImg3, DImg4;
    public Color Color_ON, Color_OFF;
    public Text DLastReceivedText, DAllReceivedText, statusText;
    //float timer = 0;
    //float timeBeforeReceive = 0;
    public float timerTimeout = 5;
    Exception exceptionInThread;
    bool exceptionError = false;



    // receiving Thread
    Thread receiveThread;

	// udpclient object
	UdpClient client;

	// public
	// public string IP = "127.0.0.1"; default local
	public int port; // define > init

	// infos
	public string lastReceivedUDPPacket="";
	public string allReceivedUDPPackets=""; // clean up this from time to time!

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
    }

    // start from unity3d
    public void Start()
    {
        //Sens1 = true;
        init();
    }

    // init
    private void init()
    {
        //print("UDPSend.init()");

        // define port
        port = 9999;

        // status
        //print("Sending to 127.0.0.1 : " + port);
        //print("Test-Sending to this Port: nc -u 127.0.0.1  " + port + "");


        receiveThread = new Thread(
            new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();
    }

    void Update(){

        //ARDUINO STATUS CHECKER
        if (exceptionError)
            Debug.LogWarning("EXCEPTION ON RECEIVE THREAD: " + exceptionInThread.ToString());

        //timer = Time.time;
        //ARDUINO STATUS CHECKER

        if (Input.GetKeyDown(debugKey))
            debugCanvas.SetActive(!debugCanvas.activeSelf);
        if (Input.GetKeyDown(invertKey))
            INVERT_INPUT = !INVERT_INPUT;
        if (debugCanvas.activeSelf)
        {
            DImg1.color = Sens1 ? Color_ON : Color_OFF;
            DImg2.color = Sens2 ? Color_ON : Color_OFF;
            DImg3.color = Sens3 ? Color_ON : Color_OFF;
            DImg4.color = Sens4 ? Color_ON : Color_OFF;
            DLastReceivedText.text = lastReceivedUDPPacket;
            DAllReceivedText.text = allReceivedUDPPackets;
            statusText.text = "ARDUINO STATUS: " + StatusCheckerServer2.Instance.Stato;
        }

        if (Sens1) {
            if (!pSens1)
            {
                pSens1 = true;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_CROGIUOLO, true);
            }
		} else {
            pSens1 = false;
        }
        if (Sens2)
        {
            if (!pSens2)
            {
                pSens2 = true;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_TRENO, true);
            }
        }
        else {
            pSens2 = false;
        }
        if (Sens3)
        {
            if (!pSens3)
            {
                pSens3 = true;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_PRESSA, true);
            }
        }
        else {
            pSens3 = false;
        }
        if (Sens4)
        {
            if (!pSens4)
            {
                pSens4 = true;
                EventAggregator.leverPressed.Invoke(Utils.LEVA_CESOIA, true);
            }
        }
        else {
            pSens4 = false;
        }

    }

	

	// receive thread
	private  void ReceiveData()
	{
        int tBefore,tAfter;
        client = new UdpClient(port);
        while (true)
        {
            // float t = Time.time;
            tBefore = System.Environment.TickCount;

            try
            {
                //IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Parse("128.32.122.125"), 9999);

                byte[] data = client.Receive(ref anyIP);

                tAfter = System.Environment.TickCount;

                if (tAfter - tBefore > (timerTimeout*1000))
                {
                    //Waiting for too long.. something is wrong!
                    StatusCheckerServer2.Instance.cambiastato("2");
                }
                else
                {
                    //Arduino is working without problems
                    StatusCheckerServer2.Instance.cambiastato("1");
                }

                string text = Encoding.UTF8.GetString(data);

                //print(">> " + text);

                // latest UDPpacket
                lastReceivedUDPPacket = text;

                // ....
                allReceivedUDPPackets = text;


                traduci(text);
            }
            catch (Exception err)
            {
                exceptionError = true;
                exceptionInThread = err;
            }
        }
    }

    // getLatestUDPPacket
    // cleans up the rest
    public string getLatestUDPPacket()
	{
		allReceivedUDPPackets="";
		return lastReceivedUDPPacket;
	}

	void OnDisable() 
	{ 
		if ( receiveThread!= null) 
			receiveThread.Abort(); 
        if(client != null)
    		client.Close(); 
	}

    void traduci(string S)
    {

        int x = 0;

        if (Int32.TryParse(S, out x))
        {
            if (Int32.TryParse(S, out x))
            {
                //Debug.Log ("eee : " + x);
                var myBitArray = new BitArray(BitConverter.GetBytes(x));

                Sens1 = myBitArray[0]; //yBitArray [0];
                Sens2 = myBitArray[1];
                Sens3 = myBitArray[2];
                Sens4 = myBitArray[3];
                if (INVERT_INPUT)
                {
                    Sens1 = !Sens1;
                    Sens2 = !Sens2;
                    Sens3 = !Sens3;
                    Sens4 = !Sens4;
                }

            }
            else
            {
                ;// Debug.Log("fallimento");
            }

        }
    }

}

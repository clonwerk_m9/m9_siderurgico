﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oscillator : MonoBehaviour {
    private float pos;
    public float speed;
    public float min, max;
	// Use this for initialization
	void Start () {
        pos = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        float frac = (Mathf.Sin(Time.time * speed)+1)/2.0f;
        float rotY = Mathf.Lerp(min, max, frac);
        transform.position = new Vector3(pos + rotY, transform.position.y, transform.position.z);
        //transform.rotation = Quaternion.Euler(transform.rotation.x, rotY, transform.rotation.z);
	}
}
